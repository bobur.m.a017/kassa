FROM openjdk:17-alpine
ADD target/kassa-kassa.jar kassa.jar
ENTRYPOINT ["java","-jar","kassa.jar"]
