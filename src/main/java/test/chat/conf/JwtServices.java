package test.chat.conf;

import io.jsonwebtoken.Claims;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;
import io.jsonwebtoken.io.Decoders;
import io.jsonwebtoken.security.Keys;
import jakarta.servlet.http.HttpServletRequest;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Component;
import test.chat.company.Company;
import test.chat.company.CompanyRepo;
import test.chat.restaurant.Restaurant;
import test.chat.user.Users;

import java.security.Key;
import java.util.*;

import static org.springframework.http.HttpHeaders.AUTHORIZATION;

@Component
@RequiredArgsConstructor
public class JwtServices {

    public String generateToken(Users user) throws IllegalArgumentException {

        Map<String, String> roles = new HashMap<>();
        roles.put("role", user.getRole().getName());
        Restaurant restaurant = user.getRestaurant();

        if (user.getRestaurant() != null) {
            roles.put("officeId", restaurant.getId().toString());
        } else if (user.getCompany() != null){
            roles.put("officeId", user.getCompany().getId().toString());
        }else {
            roles.put("officeId","not");
        }
        return Jwts.builder()
                .setClaims(roles)
                .setSubject(user.getUsername())
                .setIssuedAt(new Date())
                .setExpiration(new Date(System.currentTimeMillis() + 1000 * 60 * 60 * 24))
                .signWith(getSigningKey(), SignatureAlgorithm.HS256)
                .compact();
    }

    private Key getSigningKey() {
        String secret = "48a868a4042f634ac04a117f00a87202131dd7c46c4b32c4acb3edc5e15f4511";
        byte[] keyBytes = Decoders.BASE64.decode(secret);
        return Keys.hmacShaKeyFor(keyBytes);
    }

    public String getUsername(String token) {
        return getClaims(token).getSubject();
    }

    public String getRole(String token) {
        String s = getClaims(token).get("role", String.class);
        return s;
    }

    public Integer getOfficeId(String token) {
        String s = getClaims(token).get("officeId", String.class);
        if (s.equals("not")) {
            return 0;
        }
        return Integer.parseInt(s);
    }

    public Claims getClaims(String token) {
        return Jwts
                .parserBuilder()
                .setSigningKey(getSigningKey())
                .build()
                .parseClaimsJws(token)
                .getBody();
    }

    public String getToken(HttpServletRequest httpServletRequest) {
        String authorizationHeader = httpServletRequest.getHeader(AUTHORIZATION);
        return authorizationHeader.replace("Bearer ", "");

    }
}
