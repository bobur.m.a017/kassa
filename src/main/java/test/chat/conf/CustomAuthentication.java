package test.chat.conf;

import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.stereotype.Service;
import test.chat.user.TokenResponse;
import test.chat.user.UserRepo;
import test.chat.user.UserSignInDTO;
import test.chat.user.Users;

import java.util.Optional;

import static org.springframework.http.HttpHeaders.AUTHORIZATION;

@Service
@RequiredArgsConstructor
public class CustomAuthentication {
    private final AuthenticationManager authenticationManager;
    private final JwtServices jwtServices;
    @Autowired
    private final UserRepo userRepo;

    public TokenResponse customAuthentication(HttpServletRequest request, HttpServletResponse response) {
        String username = request.getParameter("username");
        String password = request.getParameter("password");
        Optional<Users> byUsername = userRepo.findByUsername(username);
        authenticationManager.authenticate(new UsernamePasswordAuthenticationToken(username, password));
        String token =  byUsername.map(jwtServices::generateToken).orElse(null);
        return new TokenResponse(token,jwtServices.getRole(token));

    }
    public String getRole(HttpServletRequest request) {
        String authorizationHeader = request.getHeader(AUTHORIZATION);
        String token = authorizationHeader.substring("Bearer ".length());
        return jwtServices.getRole(token);

    }

}
