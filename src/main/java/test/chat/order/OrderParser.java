package test.chat.order;

import test.chat.delivery.DeliveryDTO;
import test.chat.food.Food;
import test.chat.food.FoodDTO;
import test.chat.foodOrders.FoodOrderDTO;
import test.chat.foodOrders.FoodOrders;
import test.chat.parsers.IParsersServices;
import test.chat.restaurant.Restaurant;

import java.util.List;

public interface OrderParser extends IParsersServices {
    default OrderDTO orderToDTO(Orders orders, List<FoodOrderDTO> foodDTOList, DeliveryDTO deliveryDTO){
        return new OrderDTO(
                checkNull(orders.getId()),
                checkNull(orders.getPersonsCount()),
                checkNull(orders.getStatus()),
                orders.getType(),
                orders.getUpdatedDate(),
                foodDTOList,
                checkNull(orders.getPrices()),
                checkNull(orders.getAddress()),
                checkNull(orders.getPhoneNumber()),
                deliveryDTO
        );
    }
    default Orders DTOToOrder(OrderDTO orderDTO, List<FoodOrders> foods, String orderType, Restaurant restaurant){
        return new Orders(
                checkNull(orderDTO.getPrices()),
                checkNull(orderDTO.getCountPersons()),
                StatusOrder.UNPAID.getName(),
                foods,
                orderType,
                restaurant
        );
    }
}
