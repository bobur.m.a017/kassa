package test.chat.order;

import com.fasterxml.jackson.annotation.JsonIgnore;
import jakarta.persistence.*;
import lombok.Data;
import lombok.EqualsAndHashCode;
import test.chat.allClasses.SuperClass;
import test.chat.delivery.Delivery;
import test.chat.foodOrders.FoodOrders;
import test.chat.restaurant.Restaurant;
import test.chat.workDay.WorkDay;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.List;

@EqualsAndHashCode(callSuper = true)
@Entity
@Data
public class Orders extends SuperClass {
    @Column(precision = 19, scale = 6)
    private BigDecimal prices;
    private Integer personsCount;
    private String phoneNumber;
    private String address;
    @Column(precision = 19, scale = 6)
    private BigDecimal bodyPrice;
    private String status;
    private String type;

    @ManyToOne
    @JsonIgnore
    private Restaurant restaurant;

    @ManyToOne
    @JsonIgnore
    private WorkDay workDay;

    @ManyToOne
    private Delivery delivery;

    @OneToMany(cascade = CascadeType.ALL, fetch = FetchType.EAGER)
    @JoinColumn(referencedColumnName = "id",name = "order_id")
    private List<FoodOrders> foodOrder;

    public Orders(BigDecimal prices, Integer personsCount, String status,List<FoodOrders>foodOrder,String type,Restaurant restaurant) {
        this.prices = prices;
        this.personsCount = personsCount;
        this.status = status;
        this.foodOrder = foodOrder;
        this.type = type;
        this.restaurant = restaurant;
    }



    public Orders() {

    }

    public BigDecimal getBodyPrice(){
        BigDecimal bigDecimal = new BigDecimal(0);
        for (FoodOrders foodOrders : this.foodOrder) {
           bigDecimal = bigDecimal.add(foodOrders.getBodyPrice().multiply(BigDecimal.valueOf(foodOrders.getCount())));
        }
        return bigDecimal;
    }
}
