package test.chat.order;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
public enum StatusOrder {
    UNPAID("To'lanmagan",1),
    PAID("To'langan",2);
    private final String name;
    private final Integer num;

    StatusOrder(String name, Integer num) {
        this.name = name;
        this.num = num;
    }
}
