package test.chat.order;

import jakarta.validation.constraints.NotNull;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import test.chat.foodOrders.FoodOrderDTO;
import test.chat.warehouseOfProducts.WarehouseOfProductDTO;

import java.math.BigDecimal;
import java.sql.Timestamp;
import java.util.List;


@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class ManualOrderDTO {
    private  Integer id;
    private String type;
    @NotNull
    private WarehouseOfProductDTO warehouseOfProductDTO;
    @NotNull
    private BigDecimal prices;
    private Timestamp updateDate;





}
