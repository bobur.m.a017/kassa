package test.chat.order;

import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import test.chat.delivery.IDeliveryParser;
import test.chat.food.Food;
import test.chat.food.FoodRepo;
import test.chat.food.IFoodParser;
import test.chat.foodFromSave.FoodFromSave;
import test.chat.foodFromSave.IFoodFromSaveParser;
import test.chat.foodOrders.FoodOrderDTO;
import test.chat.foodOrders.FoodOrderParser;
import test.chat.foodOrders.FoodOrderRepo;
import test.chat.foodOrders.FoodOrders;
import test.chat.message.StateMessage;
import test.chat.outputProduct.OutputProduct;
import test.chat.outputProduct.OutputProductRepo;
import test.chat.productComposFromSave.IProductCompositionFromSaveParser;
import test.chat.productComposFromSave.ProductComposeSaveRepo;
import test.chat.productComposFromSave.ProductCompositionSave;
import test.chat.productComposition.ProductComposition;
import test.chat.restaurant.Restaurant;
import test.chat.restaurant.RestaurantRepo;
import test.chat.restaurant.StatusTypes;
import test.chat.statics.StaticMethods;
import test.chat.statusWork.StatusWork;
import test.chat.statusWork.StatusWorkRepo;
import test.chat.warehouseOfProducts.WarehouseOfProductDTO;
import test.chat.warehouseOfProducts.WarehouseOfProductRepo;
import test.chat.warehouseOfProducts.WarehouseOfProducts;
import test.chat.workDay.WorkDay;
import test.chat.workDay.WorkDayRepo;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.sql.Timestamp;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

@Service
@RequiredArgsConstructor
public class OrderServices implements IOrderServices,
        OrderParser,
        IFoodParser,
        FoodOrderParser,
        IFoodFromSaveParser,
        IProductCompositionFromSaveParser,
        IDeliveryParser {
    private final OrderRepo orderRepo;
    private final FoodRepo foodRepo;
    private final WorkDayRepo workDayRepo;
    private final RestaurantRepo restaurantRepo;
    private final StatusWorkRepo statusWorkRepo;
    private final FoodOrderRepo foodOrderRepo;
    private final ProductComposeSaveRepo productComposeSaveRepo;
    private final OutputProductRepo outputProductRepo;
    private final WarehouseOfProductRepo warehouseOfProductRepo;

    @Transactional
    @Override
    public StateMessage addOrder(OrderDTO orderDTO, String orderType, Integer restaurantId, String username) {
        StatusWork statusWork = statusWorkRepo.findByRestaurant_Id(restaurantId).orElse(null);
        if (orderDTO.getId() != null || statusWork == null) {
            return new StateMessage("Bunaqa buyurtma bor yoki ishlash rejimi aniqmas!", false, 500);
        }
        Restaurant restaurant = restaurantRepo.findById(restaurantId).orElse(null);
        if (restaurant == null) {
            return new StateMessage("Restoran topilmadi!", false, 500);
        }
        LocalDateTime date = StaticMethods.getStartCurrentDate().toLocalDateTime();
        Integer day = date.getDayOfMonth();
        Integer month = date.getMonth().getValue();
        Integer year = date.getYear();
        WorkDay workDay = workDayRepo.findByRestaurant_IdAndYearAndMonthAndDay(restaurantId, year, month, day).orElse(null);
        if (workDay == null) {
            workDay = workDayRepo.save(new WorkDay(year, month, day, restaurant, null, List.of()));

        }
        List<Integer> collect = orderDTO.getFoodOrderDTOS().stream().map((foodOrderDTO -> foodOrderDTO.getFoodDTO().getId())).toList();
        List<Food> allByIdIn = foodRepo.findAllByIdIn(collect);
        List<FoodOrders> foodOrders = new ArrayList<>();
        for (FoodOrderDTO foodOrderDTO : orderDTO.getFoodOrderDTOS()) {
            Food oneFod = getOneFod(foodOrderDTO.getFoodDTO().getId(), allByIdIn);
            WorkDay finalWorkDay = workDay;
            List<ProductCompositionSave> productCompositionSaveList = oneFod.getProductCompositions().stream().map((ProductComposition product) -> dtoToProductCompositionSave(product, statusWork.getStatus(), finalWorkDay, foodOrderDTO.getCount())).toList();
            FoodOrders foodOrders1 = DTOToFoodOrder(foodOrderDTO, DTOToFoodFromSave(oneFod, productCompositionSaveList));
            foodOrders.add(foodOrders1);
        }
        Orders ordersSave = DTOToOrder(orderDTO, foodOrders, orderType, restaurant);
        ordersSave.setUsername(username);
        ordersSave.setWorkDay(workDay);
        if (orderType.equals(TypeOrder.DELIVERY.getName())) {
            ordersSave.setPhoneNumber(orderDTO.getPhoneNumber());
            ordersSave.setAddress(orderDTO.getAddress());
        }
        Orders orders = orderRepo.save(ordersSave);
//        List<Orders> ordersList = workDay.getOrders();
//        ordersList.add(orders);
//        workDay.setOrders(ordersList);
//        workDayRepo.save(workDay);
        return new StateMessage("Buyurtma qabul qilindi!", true, 200);
    }

    @Override
    @Transactional
    public StateMessage outputProduct(WarehouseOfProductDTO warehouseOfProductDTO, Integer restaurantId, String userName) {
        Restaurant restaurant = restaurantRepo.findById(restaurantId).orElse(null);
        WarehouseOfProducts warehouseOfProducts = warehouseOfProductRepo.findById(warehouseOfProductDTO.getId()).orElse(null);
        if (warehouseOfProducts == null || restaurant == null) {
            return new StateMessage("Mahsulot topilmadi!", true, HttpStatus.NO_CONTENT.value());
        }
        StatusWork statusWork = restaurant.getStatusWork();
        if (statusWork.getStatus().equals(StatusTypes.AUTO.getName())) {
            return new StateMessage("Chiqim qilishga ruxsat yo'q!", true, HttpStatus.NOT_ACCEPTABLE.value());
        }
        StateMessage stateMessage = minusInWarehouseOfProduct(warehouseOfProducts, warehouseOfProductDTO);
        if (!stateMessage.isSuccess()) {
            return stateMessage;
        }
        if (statusWork.getDeadLine() == null || statusWork.getDeadLine().getTime() - System.currentTimeMillis() < 0) {
            statusWork.setDeadLine(new Timestamp(System.currentTimeMillis() + (1000 * 60 * 60 * 24)));
        }
        LocalDateTime date = StaticMethods.getStartCurrentDate().toLocalDateTime();
        Integer day = date.getDayOfMonth();
        Integer month = date.getMonth().getValue();
        Integer year = date.getYear();
        WorkDay workDay = workDayRepo.findByRestaurant_IdAndYearAndMonthAndDay(restaurantId, year, month, day).orElse(null);
        if (workDay == null) {
            workDay = workDayRepo.save(new WorkDay(year, month, day, warehouseOfProducts.getRestaurant(), null, List.of()));
        }
        statusWorkRepo.save(statusWork);
        ProductCompositionSave save = productComposeSaveRepo.save(
                new ProductCompositionSave(
                        warehouseOfProductDTO.getName(),
                        warehouseOfProductDTO.getWeight(),
                        warehouseOfProducts.getBodyPrice(),
                        warehouseOfProductDTO.getWeight().multiply(warehouseOfProducts.getBodyPrice()),
                        warehouseOfProducts,
                        statusWork.getStatus(),
                        workDay,
                        warehouseOfProducts.getBodyPrice()
                )
        );
        OutputProduct outputProduct = new OutputProduct();
        outputProduct.setWarehouseOfProducts(warehouseOfProducts);
        outputProduct.setWorkDay(workDay);
        outputProduct.setRestaurant(restaurant);
        outputProduct.setProductCompositionSave(save);
        outputProduct.setWeight(warehouseOfProductDTO.getWeight());
        outputProduct.setTotalPrice(warehouseOfProductDTO.getWeight().multiply(warehouseOfProducts.getBodyPrice()));
        outputProduct.setPrice(warehouseOfProducts.getBodyPrice());
        outputProductRepo.save(outputProduct);
        return stateMessage;
    }

    @Transactional
    @Override
    public StateMessage editeOutputProduct(Integer id, WarehouseOfProductDTO warehouseOfProductDTO, Integer restaurantId, String userName) {
        OutputProduct outputProduct = outputProductRepo.findById(id).orElse(null);
        Restaurant restaurant = restaurantRepo.findById(restaurantId).orElse(null);
        if (outputProduct == null || restaurant == null) {
            return new StateMessage("Mahsulot topilmadi!", true, HttpStatus.NO_CONTENT.value());
        }
        ProductCompositionSave compositionSave = outputProduct.getProductCompositionSave();
        StatusWork statusWork = restaurant.getStatusWork();
        if (statusWork.getStatus().equals(StatusTypes.AUTO.getName())) {
            return new StateMessage("Chiqim qilishga ruxsat yo'q!", true, HttpStatus.NOT_ACCEPTABLE.value());
        }
        StateMessage stateMessage = editeMinusInWarehouseOfProduct(warehouseOfProductDTO, compositionSave);
        if (!stateMessage.isSuccess()) {
            return stateMessage;
        }
        if (statusWork.getDeadLine().getTime() - System.currentTimeMillis() < 0) {
            statusWork.setDeadLine(new Timestamp(System.currentTimeMillis() + (1000 * 60 * 60 * 24)));
        }
        compositionSave.setWeight(warehouseOfProductDTO.getWeight());
        compositionSave.setPrice(warehouseOfProductDTO.getPrice());
        ProductCompositionSave save = productComposeSaveRepo.save(compositionSave);
        statusWorkRepo.save(statusWork);
        outputProduct.setProductCompositionSave(save);
        outputProduct.setWeight(warehouseOfProductDTO.getWeight());
        outputProduct.setTotalPrice(warehouseOfProductDTO.getWeight().multiply(warehouseOfProductDTO.getBodyPrice()));
        outputProduct.setPrice(warehouseOfProductDTO.getBodyPrice());
        outputProductRepo.save(outputProduct);
        return stateMessage;
    }

    public Food getOneFod(Integer foodId, List<Food> foods) {
        for (Food food : foods) {
            if (food.getId().equals(foodId)) {
                return food;
            }
        }
        return null;
    }


    @Override
    public StateMessage editeOrder(OrderDTO orderDTO) {
        Orders orders = orderRepo.findById(orderDTO.getId()).orElse(null);
        if (orders == null) {
            return new StateMessage("Buyurtma topilmadi!", false, 500);
        }
        StatusWork statusWork = statusWorkRepo.findByRestaurant_Id(orders.getRestaurant().getId()).orElse(null);
        if (statusWork == null) {
            return new StateMessage("Ishlash rejimi aniqmas!", false, 500);
        }
        LocalDateTime date = StaticMethods.getStartCurrentDate().toLocalDateTime();
        Integer day = date.getDayOfMonth();
        Integer month = date.getMonth().getValue();
        Integer year = date.getYear();
        WorkDay workDay = workDayRepo.findByRestaurant_IdAndYearAndMonthAndDay(orders.getRestaurant().getId(), year, month, day).orElse(null);
        if (workDay == null) {
            return new StateMessage("Buyurtma topilmadi!", false, HttpStatus.NO_CONTENT.value());
        }
        List<FoodOrders> foodOrders = new ArrayList<>();
        for (FoodOrderDTO foodOrderDTO : orderDTO.getFoodOrderDTOS()) {
            if (foodOrderDTO.getId() == null) {
                Food food = foodRepo.findById(foodOrderDTO.getFoodDTO().getId()).orElse(null);
                if (food == null) {
                    return new StateMessage("Taom topilmadi!", false, 500);
                }
                FoodOrders foodOrders1 = DTOToFoodOrder(foodOrderDTO, DTOToFoodFromSave(food, dtoToProductCompositionSaveList(food.getProductCompositions(), statusWork.getStatus(), workDay, foodOrderDTO.getCount())));
                foodOrderRepo.save(foodOrders1);
                foodOrders.add(foodOrders1);
            }
//            foodOrders.add(DTOToFoodOrder(foodOrderDTO, null));
        }
        orders.setFoodOrder(foodOrders);
        orderRepo.save(orders);
        return new StateMessage("Buyurtma qabul qilindi!", true, 200);
    }

    @Override
    public StateMessage deleteOrder(Integer id) {
        orderRepo.deleteById(id);
        return new StateMessage("O'chirildi", true, 200);
    }

    @Override
    @Transactional
    public StateMessage deleteFoodOrder(Integer id) {
        foodOrderRepo.deleteFoodOrdersByIdqqqq(id);
        return new StateMessage("O'chirildi", true, 200);
    }

    @Transactional
    @Override
    public StateMessage changeStatus(Integer id, Integer restaurantId) {
        Orders orders = orderRepo.findById(id).orElse(null);
        if (orders == null || orders.getStatus().equals(StatusOrder.PAID.getName())) {
            return new StateMessage("Bajarilmadi", true, 417);
        }
        Restaurant restaurant = restaurantRepo.findById(restaurantId).orElse(null);
        if (restaurant == null) {
            return new StateMessage("Restaurant topilmadi", true, 417);
        }
        StatusWork statusWork = restaurant.getStatusWork();
        if (statusWork.getStatus().equals(StatusTypes.AUTO_MANUAL.getName()) || statusWork.getStatus().equals(StatusTypes.AUTO.getName())) {
            if ((statusWork.getDeadLine() == null || statusWork.getDeadLine().getTime() - System.currentTimeMillis() < 0) || statusWork.getStatus().equals(StatusTypes.AUTO.getName())) {
                if (calculateFoodProduct(orders)) {
                    orders.setStatus(StatusOrder.PAID.getName());
                    orderRepo.save(orders);
                    return new StateMessage("Bajarildi", true, 200);
                }
            }
        }
        orders.setStatus(StatusOrder.PAID.getName());
        orderRepo.save(orders);
        return new StateMessage("Bajarilmadi", true, 417);
    }

    @Override
    public List<OrderDTO> getOrders(Long startDate, Long endDate, String orderType, Integer restaurantId) {
        Timestamp start = StaticMethods.getStartCurrentDate();
        if (startDate != null) {
            start = new Timestamp(startDate);
        }
        Timestamp end = StaticMethods.addDayToMillisecond(1);
        if (endDate != null) {
            end = new Timestamp(endDate);
        }

        List<Orders> ordersList = orderRepo.findAllByUpdatedDateAndRestaurant_IdAndType(start, end, restaurantId, orderType);
        List<OrderDTO> orderDTOS = new ArrayList<>();
        for (Orders orders : ordersList) {
            List<FoodOrderDTO> foodDTOList = new ArrayList<>();
            for (FoodOrders food : orders.getFoodOrder()) {
                foodDTOList.add(foodOrderToDTO(food, foodSaveToDTO(food.getFoodFromSave(), null)));
            }
            orderDTOS.add(orderToDTO(orders, foodDTOList, deliveryToDTO(checkNull(orders.getDelivery()))));
        }
        return orderDTOS;
    }

    @Override
    public OrderDTO getOrder(Integer id) {
        Orders orders = orderRepo.findById(id).orElse(null);
        if (orders == null) {
            return null;
        }
        List<FoodOrderDTO> foodDTOList = new ArrayList<>();
        for (FoodOrders food : orders.getFoodOrder()) {
            foodDTOList.add(foodOrderToDTO(food, foodSaveToDTO(food.getFoodFromSave(), null)));
        }
        return orderToDTO(orders, foodDTOList, deliveryToDTO(checkNull(orders.getDelivery())));
    }

    public synchronized Boolean calculateFoodProduct(Orders orders) {
        for (FoodOrders foodOrders : orders.getFoodOrder()) {
            if (!warehouseProductMinus(foodOrders.getFoodFromSave(), foodOrders.getCount(), orders.getRestaurant(), orders.getWorkDay())) {
                return false;
            }
        }
        return true;
    }

    public Boolean warehouseProductMinus(FoodFromSave food, Integer count, Restaurant restaurant, WorkDay workDay) {
        for (ProductCompositionSave productComposition : food.getProductCompositionSaves()) {
            WarehouseOfProducts warehouseOfProducts = warehouseOfProductRepo.findById(productComposition.getWarehouseOfProducts().getId()).orElse(null);
            if (warehouseOfProducts == null) {
                return false;
            }
            BigDecimal weight = productComposition.getWeight();
            if (warehouseOfProducts.getWeight().setScale(4, RoundingMode.HALF_EVEN).compareTo(weight) > -1) {
                OutputProduct outputProduct = new OutputProduct();
                warehouseOfProducts.setWeight(warehouseOfProducts.getWeight().subtract(weight));
                outputProduct.setWarehouseOfProducts(warehouseOfProducts);
                outputProduct.setWeight(weight);
                outputProduct.setPrice(warehouseOfProducts.getBodyPrice());
                outputProduct.setRestaurant(restaurant);
                outputProduct.setWorkDay(workDay);
                outputProductRepo.save(outputProduct);
            } else {
                warehouseOfProducts.setTotalPrice(BigDecimal.ZERO);
                warehouseOfProducts.setWeight(BigDecimal.valueOf(0));
            }
            warehouseOfProductRepo.save(warehouseOfProducts);
        }
        return true;
    }

    public StateMessage minusInWarehouseOfProduct(WarehouseOfProducts warehouseOfProducts, WarehouseOfProductDTO warehouseOfProductDTO) {
        if (warehouseOfProducts.getWeight().compareTo(warehouseOfProductDTO.getWeight()) > -1) {
            warehouseOfProducts.setWeight(warehouseOfProducts.getWeight().subtract(warehouseOfProductDTO.getWeight()));
            warehouseOfProductRepo.save(warehouseOfProducts);
            return new StateMessage("Mahsulot chiqim bo'ldi", true, 200);
        }
        return new StateMessage("Mahsulot yetmadi", false, 417);
    }

    public StateMessage editeMinusInWarehouseOfProduct(WarehouseOfProductDTO warehouseOfProductDTO, ProductCompositionSave productCompositionSave) {
        WarehouseOfProducts warehouseOfProducts = productCompositionSave.getWarehouseOfProducts();
        BigDecimal added = warehouseOfProducts.getWeight().add(productCompositionSave.getWeight());
        BigDecimal addedPrice = warehouseOfProducts.getBodyPrice().add(productCompositionSave.getBodyPrice());
//        if (!addedPrice.equals(BigDecimal.ZERO) && !added.equals(BigDecimal.ZERO)){
//            addedPrice = w
//        }
        warehouseOfProducts.setWeight(added);
        warehouseOfProducts.setBodyPrice(addedPrice.divide(added, RoundingMode.HALF_EVEN));
        if (warehouseOfProducts.getWeight().compareTo(warehouseOfProductDTO.getWeight()) > -1) {
            warehouseOfProducts.setWeight(warehouseOfProducts.getWeight().subtract(warehouseOfProductDTO.getWeight()));
            warehouseOfProductRepo.save(warehouseOfProducts);
            return new StateMessage("Mahsulot o'zgardi", true, 200);
        }
        return new StateMessage("Mahsulot yetmadi", false, 417);
    }
}
