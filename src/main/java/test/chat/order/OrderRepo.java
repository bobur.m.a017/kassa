package test.chat.order;

import jakarta.persistence.PersistenceContext;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import test.chat.restaurant.StatusTypes;
import test.chat.workDay.WorkDay;

import java.sql.Timestamp;
import java.util.List;

@Repository
public interface OrderRepo extends JpaRepository<Orders,Integer> {
    @Query("SELECT o FROM Orders o WHERE  ( o.updatedDate > :startDate or cast(:startDate as timestamp ) is null ) AND ( o.updatedDate < :endDate or cast(:endDate as timestamp ) is null) AND  o.restaurant.id = :restaurantId AND (cast(:orderType  as string ) is  null  or o.type = :orderType)")
    List<Orders> findAllByUpdatedDateAndRestaurant_IdAndType(@Param("startDate") Timestamp startDate,@Param("endDate") Timestamp endDate,@Param("restaurantId") Integer restaurantId,@Param("orderType") String orderType);
    List<Orders> findAllByWorkDayInAndStatus(List<WorkDay> workDays,String status);
    List<Orders> findAllByWorkDay_IdAndStatus(Integer id,String status);

}