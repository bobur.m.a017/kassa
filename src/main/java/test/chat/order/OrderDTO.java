package test.chat.order;

import jakarta.validation.constraints.NotNull;
import lombok.*;
import test.chat.delivery.DeliveryDTO;
import test.chat.food.FoodDTO;
import test.chat.foodOrders.FoodOrderDTO;

import java.lang.Float;
import java.math.BigDecimal;
import java.sql.Timestamp;
import java.util.List;


@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class OrderDTO {
    private  Integer id;
    private  Integer countPersons;
    private String status;
    private String type;
    private Timestamp updateDate;
    @NotNull
    private List<FoodOrderDTO> foodOrderDTOS;
    @NotNull
    private BigDecimal prices;
    private String address;
    private String phoneNumber;
    private DeliveryDTO deliveryDTO;

    public OrderDTO(Integer id, Integer countPersons, String status, BigDecimal prices,List<FoodOrderDTO> foodOrderDTOS,Timestamp updateDate,String type,DeliveryDTO deliveryDTO) {
        this.id = id;
        this.countPersons = countPersons;
        this.status = status;
        this.prices = prices;
        this.foodOrderDTOS = foodOrderDTOS;
        this.updateDate = updateDate;
        this.type = type;
        this.deliveryDTO = deliveryDTO;
    }


}
