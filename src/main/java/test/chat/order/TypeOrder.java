package test.chat.order;

import lombok.Getter;

@Getter
public enum TypeOrder {
    TREASURY("G'azna",1),
    DELIVERY("Yetkazish",2);
    private final String name;
    private final Integer num;

    TypeOrder(String name, Integer num) {
        this.name = name;
        this.num = num;
    }
}
