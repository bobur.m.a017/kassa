package test.chat.order;

import test.chat.message.StateMessage;
import test.chat.warehouseOfProducts.WarehouseOfProductDTO;

import java.util.List;

public interface IOrderServices {
    StateMessage addOrder(OrderDTO orderDTO,String OrderType,Integer restaurantId,String userName);
    StateMessage outputProduct(WarehouseOfProductDTO warehouse, Integer restaurantId, String userName);
    StateMessage editeOutputProduct(Integer id,WarehouseOfProductDTO warehouse, Integer restaurantId, String userName);
    StateMessage editeOrder(OrderDTO orderDTO);
    StateMessage deleteOrder(Integer id);
    StateMessage deleteFoodOrder(Integer id);
    StateMessage changeStatus(Integer id,Integer restaurantId);
    List<OrderDTO> getOrders(Long startDate,Long endDate,String orderType,Integer restaurantId);
    OrderDTO getOrder(Integer id);

}
