package test.chat.foodFromSave;

import test.chat.food.Food;
import test.chat.parsers.IParsersServices;
import test.chat.productComposition.ProductCompositionDTO;
import test.chat.productComposFromSave.ProductCompositionSave;
import test.chat.statics.StaticWords;

import java.util.List;

public interface IFoodFromSaveParser extends IParsersServices {
    default FoodFromSaveDTO foodFromSaveToDTO(Food food, List<ProductCompositionDTO> productCompositionDTOS) {
        return new FoodFromSaveDTO(
                food.getId(),
                food.getName(),
                food.getPrice(),
                checkNull(food.getDescription()),
                (food.getAttachment() != null ? food.getAttachment().getName() : null) != null ? StaticWords.URI + (food.getAttachment() != null ? food.getAttachment().getName() : null) : null,
                checkNull(food.getCategory()),
                productCompositionDTOS
        );
    }

    default FoodFromSave DTOToFoodFromSave(Food food, List<ProductCompositionSave> productCompositionSaves) {
        return new FoodFromSave(
                food.getName(),
                checkNull(food.getDescription()),
                food.getPrice(),
                food.getBodyPrice(),
                productCompositionSaves
        );
    }
}
