package test.chat.foodFromSave;

import jakarta.persistence.*;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import test.chat.allClasses.SuperClass;
import test.chat.foodOrders.FoodOrders;
import test.chat.productComposFromSave.ProductCompositionSave;

import java.math.BigDecimal;
import java.util.List;

@Entity
@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
public class FoodFromSave extends SuperClass {
    @Column(nullable = false)
    private String name;
    private String description;
    @Column(nullable = false,precision = 19, scale = 6)
    private BigDecimal price;
    @Column(nullable = false,precision = 19, scale = 6)
    private BigDecimal bodyPrice = new BigDecimal(0);

    @OneToOne(mappedBy = "foodFromSave")
    @JoinColumn(name = "food_from_save_id", referencedColumnName = "id")
    private FoodOrders foodOrders;

    @OneToMany(fetch = FetchType.EAGER,cascade = CascadeType.ALL)
    @JoinColumn(name = "food from_save_id", referencedColumnName = "id")
    private List<ProductCompositionSave> productCompositionSaves;

    public FoodFromSave(String name, String description, BigDecimal price, BigDecimal bodyPrice, List<ProductCompositionSave> productCompositions) {
        this.name = name;
        this.description = description;
        this.price = price;
        this.bodyPrice = bodyPrice;
        this.productCompositionSaves = productCompositions;
    }

    public BigDecimal getBodyPrice() {
            BigDecimal bigDecimal = new BigDecimal(0);
        if (this.productCompositionSaves != null) {
            for (ProductCompositionSave productComposition : this.productCompositionSaves) {
                bigDecimal = bigDecimal.add(productComposition.getPrice().multiply(productComposition.getWeight()));
            }
        }
        return bigDecimal;
    }
}
