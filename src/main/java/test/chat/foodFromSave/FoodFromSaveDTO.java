package test.chat.foodFromSave;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import test.chat.category.Category;
import test.chat.productComposition.ProductCompositionDTO;

import java.math.BigDecimal;
import java.util.List;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class FoodFromSaveDTO {
    private Integer id;
    private String name;
    private String imgUrl;
    private BigDecimal price;
    private String description;
    private Category category;
    private List<ProductCompositionDTO> productCompositionDTOS;
    public FoodFromSaveDTO(Integer id, String name, BigDecimal price, String description, String imgUrl, Category category, List<ProductCompositionDTO> productCompositionDTOS) {
        this.id = id;
        this.name = name;
        this.imgUrl = imgUrl;
        this.price = price;
        this.description = description;
        this.category = category;
        this.productCompositionDTOS = productCompositionDTOS;
    }
}
