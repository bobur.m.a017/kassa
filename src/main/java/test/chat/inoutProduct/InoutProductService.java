package test.chat.inoutProduct;

import lombok.RequiredArgsConstructor;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import test.chat.message.StateMessage;
import test.chat.restaurant.Restaurant;
import test.chat.restaurant.RestaurantRepo;
import test.chat.restaurant.StatusTypes;
import test.chat.statics.StaticMethods;
import test.chat.statusWork.StatusWork;
import test.chat.statusWork.StatusWorkRepo;
import test.chat.user.UserRepo;
import test.chat.user.Users;
import test.chat.warehouseOfProducts.IWarehouseOfProductParser;
import test.chat.warehouseOfProducts.WarehouseOfProductRepo;
import test.chat.warehouseOfProducts.WarehouseOfProducts;
import test.chat.workDay.DayWareHouseOfProducts;
import test.chat.workDay.WorkDay;
import test.chat.workDay.WorkDayRepo;
import test.chat.workDay.WorkDayWareHoseOfProductRepo;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.sql.Timestamp;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

@Service
@RequiredArgsConstructor
@EnableScheduling
public class InoutProductService implements IInoutProductServices, IInoutProductParser, IWarehouseOfProductParser {
    private final InoutProductRepo inoutProductRepo;
    private final UserRepo userRepo;
    private final WorkDayRepo workDayRepo;
    private final StatusWorkRepo statusWorkRepo;
    private final RestaurantRepo restaurantRepo;
    private final WarehouseOfProductRepo warehouseOfProductRepo;
    private final WorkDayWareHoseOfProductRepo workDayWareHoseOfProductRepo;

    @Transactional
    @Override
    public StateMessage addInoutProduct(InoutProductDTO inoutProductDTO, Integer productId, String userName, Integer restaurantId) {
        if (inoutProductDTO.getId() != null) {
            return new StateMessage("Yangi mahsulot kirim qiling", false, 404);
        } else if (inoutProductDTO.getWeight().compareTo(BigDecimal.ZERO) == 0) {
            return new StateMessage("Mahsulot vazni 0 ga teng", false, 404);
        }
        WarehouseOfProducts warehouseOfProducts = warehouseOfProductRepo.findById(productId).orElse(null);
        Restaurant restaurant = restaurantRepo.findById(restaurantId).orElse(null);
        LocalDateTime date = StaticMethods.getStartCurrentDate().toLocalDateTime();
        Integer day = date.getDayOfMonth();
        Integer month = date.getMonth().getValue();
        Integer year = date.getYear();
        if (warehouseOfProducts == null || restaurant == null) {
            return new StateMessage("Mahsulot turi yoki restoran topilmadi", false, 404);
        }
        Users users = userRepo.findByUsername(userName).orElse(null);
        BigDecimal bigDecimal = totalWeightWarehouseProduct(inoutProductDTO, warehouseOfProducts);
        if (bigDecimal == null) {
            return new StateMessage("Mahsulot turi topilmadi", false, 404);
        }
        InoutProduct inoutProduct = dtoToInoutProduct(inoutProductDTO, bigDecimal, restaurant, warehouseOfProducts);
        if (users != null) {
            inoutProduct.setUsername(users.getUsername());
        }
        WorkDay workDay = workDayRepo.findByRestaurant_IdAndYearAndMonthAndDay(restaurantId, year, month, day).orElse(null);
        if (workDay == null) {
            workDay = new WorkDay(year, month, day, restaurant, List.of(inoutProduct), null);
        } else {
            List<InoutProduct> inoutProductList = workDay.getInoutProduct();
            inoutProductList.add(inoutProduct);
            workDay.setInoutProduct(inoutProductList);
        }

        workDay = workDayRepo.save(workDay);
        inoutProduct.setWorkDay(workDay);
        inoutProductRepo.save(inoutProduct);
        return new StateMessage("Mahsulot kirim bo'ldi", true, 200);
    }

    @Transactional
    @Override
    public StateMessage editeInoutProduct(InoutProductDTO inoutProductDTO) {
        InoutProduct inoutProduct = inoutProductRepo.findById(inoutProductDTO.getId()).orElse(null);
        WarehouseOfProducts warehouseOfProducts = warehouseOfProductRepo.findById(inoutProductDTO.getProductDTO().getId()).orElse(null);
        if (inoutProduct == null || warehouseOfProducts == null) {
            return new StateMessage("Mahsulot turi topilmadi", false, 404);
        }
        if (inoutProductDTO.getWeight().compareTo(BigDecimal.ZERO) == 0) {
            return new StateMessage("Mahsulot vazni 0 ga teng", false, 404);
        }
        long l = System.currentTimeMillis() - inoutProduct.getUpdatedDate().getTime();
//        if (l > (1000 * 60 * 60)) {
//            return new StateMessage("O'zgartirish mumkin emas, belgilangan vaqtdan o'tib ketgan", false, 404);
//        }
        if (!inoutProduct.getWarehouseOfProducts().getId().equals(inoutProductDTO.getProductDTO().getId())) {
            return new StateMessage("Mahsulot topilmadi", false, 404);
        }
        Boolean addOrEdite = minusTotalWeightWarehouseProduct(warehouseOfProducts, inoutProduct);
        if (!addOrEdite) {
            return new StateMessage("Mahsulot miqdori yetmaydi", false, 404);
        }
        BigDecimal bigDecimal = totalWeightWarehouseProduct(inoutProductDTO, warehouseOfProducts);
        inoutProduct.setWarehouseOfProducts(warehouseOfProducts);
        inoutProduct.setPrice(new BigDecimal(inoutProductDTO.getPrice().toString()).setScale(6, RoundingMode.HALF_EVEN));
        inoutProduct.setWeight(new BigDecimal(inoutProductDTO.getWeight().toString()).setScale(6, RoundingMode.HALF_EVEN));
        inoutProduct.setTotalPrice(new BigDecimal(inoutProductDTO.getTotalPrice().toString()).setScale(6, RoundingMode.HALF_EVEN));
        inoutProduct.setTotalWeight(bigDecimal);
        inoutProductRepo.save(inoutProduct);
        return new StateMessage("Mahsulot kirim bo'ldi", true, 200);
    }

    @Override
    @Transactional
    public StateMessage deleteInoutProduct(Integer id) {
        inoutProductRepo.deleteById(id);
        return new StateMessage("Ochirildi", true, 200);
    }

    @Override
    public InoutProductDTO getInoutProduct(Integer inoutProductId) {
        InoutProduct inoutProduct = inoutProductRepo.findById(inoutProductId).orElse(null);
        if (inoutProduct == null) {
            return null;
        }
        return inoutProductToDTO(inoutProduct, wareHouseProductToDTO(inoutProduct.getWarehouseOfProducts()));
    }

    @Override
    public List<InoutProductDTO> getInoutProducts(Long startDate, Long endDate, Integer restaurantId) {
        Timestamp start = StaticMethods.getStartCurrentDate();
        if (startDate != null) {
            start = new Timestamp(startDate);
        }
        Timestamp end = StaticMethods.addDayToMillisecond(2);
        if (endDate != null) {
            end = new Timestamp(endDate);
        }
        List<InoutProductDTO> inoutProductDTOS = new ArrayList<>();
//        for (InoutProduct inoutProduct : inoutProductRepo.findAllByInoutProductByRestaurantAndDate(restaurantId, start, end)) {
        for (InoutProduct inoutProduct : inoutProductRepo.findAll()) {
            inoutProductDTOS.add(inoutProductToDTO(inoutProduct, wareHouseProductToDTO(inoutProduct.getWarehouseOfProducts())));
        }
        return inoutProductDTOS;
    }

    public BigDecimal totalWeightWarehouseProduct(InoutProductDTO inoutProductDTO, WarehouseOfProducts warehouseOfProduct) {
        warehouseOfProduct.setBodyPrice(calculateBodyPrice(warehouseOfProduct, inoutProductDTO));
        warehouseOfProduct.setPrice(inoutProductDTO.getPrice());
        BigDecimal add = warehouseOfProduct.getWeight().add(inoutProductDTO.getWeight());
        warehouseOfProduct.setWeight(add);
        warehouseOfProductRepo.save(warehouseOfProduct);
        return add;
    }


    public Boolean minusTotalWeightWarehouseProduct(WarehouseOfProducts warehouseOfProduct, InoutProduct inoutProduct) {
        if (warehouseOfProduct.getWeight().compareTo(inoutProduct.getWeight()) < 0) {
            return false;
        }
        BigDecimal totalPriceInWarehouse = warehouseOfProduct.getTotalPrice().subtract(
                inoutProduct.getPrice().multiply(inoutProduct.getWeight())
        );
        BigDecimal add = warehouseOfProduct.getWeight().subtract(inoutProduct.getWeight());
        warehouseOfProduct.setWeight(add);
        if (add.compareTo(BigDecimal.ZERO) > 0) {
            warehouseOfProduct.setPrice(totalPriceInWarehouse.divide(add, 4, RoundingMode.HALF_EVEN));
            warehouseOfProduct.setBodyPrice(totalPriceInWarehouse.divide(add, 4, RoundingMode.HALF_EVEN));
        } else {
            warehouseOfProduct.setPrice(add);
            warehouseOfProduct.setBodyPrice(add);
        }
        warehouseOfProductRepo.save(warehouseOfProduct);
        return true;
    }

    @Scheduled(cron = "0 30 23 * * *", zone = "Asia/Tashkent")
    public void everyday() {
        LocalDateTime date = StaticMethods.getStartCurrentDate().toLocalDateTime();
        Integer day = date.getDayOfMonth();
//        LocalDateTime dayLocal = date.minusDays(2);
//        Integer day = dayLocal.getDayOfMonth();
        Integer month = date.getMonth().getValue();
        Integer year = date.getYear();

        for (WorkDay workDay : workDayRepo.findAllByYearAndMonthAndDay(year, month, day)) {
            List<DayWareHouseOfProducts> warehouseOfProductsList = new ArrayList<>();
            List<WarehouseOfProducts> warehouseOfProducts = warehouseOfProductRepo.findAllByRestaurant_IdOrderByName(workDay.getRestaurant().getId());
            for (WarehouseOfProducts warehouseOfProduct : warehouseOfProducts) {
                if (warehouseOfProduct.getWeight().compareTo(BigDecimal.ZERO) > 0) {
                    warehouseOfProductsList.add(
                            new DayWareHouseOfProducts(
                                    warehouseOfProduct.getName(),
                                    warehouseOfProduct.getWeight(),
                                    warehouseOfProduct.getBodyPrice(),
                                    warehouseOfProduct.getTotalPrice(),
                                    workDay,
                                    warehouseOfProduct
                            )
                    );
                }
            }
            workDayWareHoseOfProductRepo.saveAll(warehouseOfProductsList);
        }

    }

    public BigDecimal calculateBodyPrice(WarehouseOfProducts warehouse, InoutProductDTO inoutProductDTO) {
        BigDecimal weight = warehouse.getWeight().add(inoutProductDTO.getWeight());
        BigDecimal price = warehouse.getTotalPrice().add(inoutProductDTO.getPrice().multiply(inoutProductDTO.getWeight()));
        return price.divide(weight, RoundingMode.HALF_EVEN);
    }

    public BigDecimal calculateMinusBodyPrice(WarehouseOfProducts warehouse, InoutProduct inoutProductDTO) {
        BigDecimal weight = warehouse.getWeight().subtract(inoutProductDTO.getWeight());
        BigDecimal price = warehouse.getPrice().subtract(inoutProductDTO.getPrice());
        return price.divide(weight, RoundingMode.HALF_EVEN);
    }

}
