package test.chat.inoutProduct;

import test.chat.message.StateMessage;

import java.sql.Timestamp;
import java.util.List;

public interface IInoutProductServices {
    StateMessage addInoutProduct(InoutProductDTO inoutProductDTO, Integer productId, String userName, Integer restaurantId);

    StateMessage editeInoutProduct(InoutProductDTO inoutProductDTO);

    StateMessage deleteInoutProduct(Integer id);

    InoutProductDTO getInoutProduct(Integer inoutProductId);

    List<InoutProductDTO> getInoutProducts(Long startDate, Long endDate, Integer restaurantId);
    void everyday();
}
