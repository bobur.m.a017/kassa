package test.chat.inoutProduct;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;
import test.chat.workDay.WorkDay;

import java.sql.Timestamp;
import java.util.List;

@Repository
public interface InoutProductRepo extends JpaRepository<InoutProduct, Integer> {

    @Query("SELECT o FROM InoutProduct o WHERE o.restaurant.id = ?1 AND ( ?2 IS NULL OR o.updatedDate > ?2 )AND (?3 IS NULL OR o.updatedDate < ?3)")
    List<InoutProduct> findAllByInoutProductByRestaurantAndDate(Integer restaurantId, Timestamp startDate, Timestamp endDate);

    List<InoutProduct> findAllByWorkDayIn(List<WorkDay> workDays);
    List<InoutProduct> findAllByWorkDay_Id(Integer id);

    @Override
    @Modifying
    void deleteById(Integer id);
}
