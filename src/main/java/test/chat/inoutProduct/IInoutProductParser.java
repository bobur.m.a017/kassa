package test.chat.inoutProduct;

import test.chat.restaurant.Restaurant;
import test.chat.warehouseOfProducts.WarehouseOfProductDTO;
import test.chat.warehouseOfProducts.WarehouseOfProducts;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.List;

public interface IInoutProductParser {
    default InoutProduct dtoToInoutProduct(InoutProductDTO inoutProductDTO, BigDecimal totalWeight, Restaurant restaurant, WarehouseOfProducts warehouse){
        return new InoutProduct(
                inoutProductDTO.getName(),
                new BigDecimal(inoutProductDTO.getWeight().toString()).setScale(6, RoundingMode.HALF_EVEN),
                totalWeight,
                new BigDecimal(inoutProductDTO.getPrice().toString()).setScale(6, RoundingMode.HALF_EVEN),
                new BigDecimal(inoutProductDTO.getTotalPrice().toString()).setScale(6, RoundingMode.HALF_EVEN),
                restaurant,
                warehouse
        );
    }
    default InoutProductDTO inoutProductToDTO(InoutProduct inoutProduct, WarehouseOfProductDTO productDTO){
        return new InoutProductDTO(
                inoutProduct.getId(),
                inoutProduct.getName(),
                inoutProduct.getWeight(),
                inoutProduct.getPrice(),
                inoutProduct.getTotalPrice(),
                inoutProduct.getTotalWeight(),
                productDTO,
                inoutProduct.getUpdatedDate()
        );
    }

}
