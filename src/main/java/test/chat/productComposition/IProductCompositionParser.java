package test.chat.productComposition;
import test.chat.food.Food;
import test.chat.warehouseOfProducts.WarehouseOfProductDTO;
import test.chat.warehouseOfProducts.WarehouseOfProducts;

import java.math.BigDecimal;
import java.math.MathContext;
import java.math.RoundingMode;
import java.util.ArrayList;
import java.util.List;

public interface IProductCompositionParser {
    default ProductComposition dtoToProductComposition(ProductCompositionDTO productDTO, Food food, WarehouseOfProducts warehouseOfProducts){
        return new ProductComposition(
                new BigDecimal(productDTO.getWeight().toString()).setScale(4, RoundingMode.HALF_EVEN),
                food,
                warehouseOfProducts
        );
    }
    default ProductCompositionDTO productCompToDTO(ProductComposition productCompositions, WarehouseOfProductDTO productDTO){
        return new ProductCompositionDTO(
                productCompositions.getId(),
                productCompositions.getWeight(),
                productCompositions.getWarehouseOfProducts().getBodyPrice(),
                null,
                productDTO
        );
    }
    default List<ProductCompositionDTO> productCompToDTOList(List<ProductComposition> productCompositions){
        List<ProductCompositionDTO> compositionDTOS = new ArrayList<>();
        for (ProductComposition productComposition : productCompositions) {
            compositionDTOS.add(productCompToDTO(productComposition,new WarehouseOfProductDTO(productComposition.getWarehouseOfProducts().getId(),
                    productComposition.getWarehouseOfProducts().getName(),
                    productComposition.getWarehouseOfProducts().getWeight(),
                    productComposition.getWarehouseOfProducts().getBodyPrice(),
                    productComposition.getWarehouseOfProducts().getPrice(),
                    productComposition.getWarehouseOfProducts().getTotalPrice()
            )));
        }
        return compositionDTOS;
    }

}
