package test.chat.productComposition;

import test.chat.message.StateMessage;

import java.util.List;

public interface IProductCompositionServices {
    StateMessage addProductComposition(ProductCompositionDTO productCompositionDTO, String userName);
    StateMessage editeProductComposition(ProductCompositionDTO productCompositionDTO, String userName);
    StateMessage deleteProductComposition(Integer id);
    ProductCompositionDTO getProductComposition(Integer productCompositionId);
    List<ProductCompositionDTO> getProductCompositions();
}
