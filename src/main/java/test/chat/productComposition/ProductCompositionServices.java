package test.chat.productComposition;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import test.chat.message.StateMessage;
import test.chat.user.UserRepo;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Service
@RequiredArgsConstructor
public class ProductCompositionServices implements IProductCompositionServices, IProductCompositionParser {
    private final ProductCompositionRepo productCompositionRepo;
    private final UserRepo userRepo;

    @Override
    public StateMessage addProductComposition(ProductCompositionDTO productDTO, String userName) {
//        Users users = userRepo.findByUsername(userName).orElse(null);
//        if (users != null) {
//            productCompositionRepo.save(dtoToProductComposition(productDTO));
//            return new StateMessage("Qo'shildi", true, 200);
//        }
        return new StateMessage("Qo'shilmadi", true, 500);
    }

    @Override
    public StateMessage editeProductComposition(ProductCompositionDTO productDTO, String userName) {
        Optional<ProductComposition> productOptional = productCompositionRepo.findById(productDTO.getId());
        ProductComposition product;
        if (productOptional.isPresent()) {
            product = productOptional.get();
            productCompositionRepo.save(product);
            return new StateMessage("O'zgartirildi", true, 200);
        }
        return new StateMessage("O'zgartirilmadi", true, 500);
    }

    @Override

    public StateMessage deleteProductComposition(Integer id) {
        try {
            productCompositionRepo.deleteById(id);
            return new StateMessage("O'chirildi", true, 200);
        } catch (Exception e) {
            return new StateMessage("O'chirilmadi", true, 500);
        }
    }

    @Override
    public ProductCompositionDTO getProductComposition(Integer productId) {
        Optional<ProductComposition> optionalProductComposition = productCompositionRepo.findById(productId);

        return null;
    }

    @Override
    public List<ProductCompositionDTO> getProductCompositions() {
        List<ProductComposition> products = productCompositionRepo.findAll();
        List<ProductCompositionDTO> productDTOS = new ArrayList<>();
        for (ProductComposition product : products) {
//            productDTOS.add(productCompToDTO(product,productToDTO(product.getWarehouseOfProducts().getProduct())));
        }
        return productDTOS;
    }

}
