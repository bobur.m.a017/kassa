package test.chat.productComposition;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import test.chat.warehouseOfProducts.WarehouseOfProductDTO;

import java.math.BigDecimal;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class ProductCompositionDTO {
    private Integer id;
    private BigDecimal weight;
    private BigDecimal price;
    private BigDecimal totalPrice;

    private WarehouseOfProductDTO productDTO;
}
