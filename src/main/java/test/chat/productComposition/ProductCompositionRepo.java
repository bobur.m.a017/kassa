package test.chat.productComposition;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

@Repository
public interface ProductCompositionRepo extends JpaRepository<ProductComposition, Integer> {
    @Modifying
    @Query("DELETE FROM ProductComposition b WHERE b.id = ?1")
    void deleteByIdComp(Integer id);
}
