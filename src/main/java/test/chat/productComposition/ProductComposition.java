package test.chat.productComposition;

import com.fasterxml.jackson.annotation.JsonIgnore;
import jakarta.persistence.*;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import test.chat.allClasses.SuperClass;
import test.chat.food.Food;
import test.chat.warehouseOfProducts.WarehouseOfProducts;
import java.math.BigDecimal;

@Entity
@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
public class ProductComposition extends SuperClass {
    @Column(nullable = false,precision = 19, scale = 6)
    private BigDecimal weight;

    @ManyToOne(optional = false)
    @JsonIgnore
    private Food food;

    @ManyToOne(optional = false)
    @JsonIgnore
    private WarehouseOfProducts warehouseOfProducts;
}