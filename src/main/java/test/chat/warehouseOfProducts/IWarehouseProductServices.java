package test.chat.warehouseOfProducts;
import test.chat.message.StateMessage;
import java.util.List;


public interface IWarehouseProductServices {
    StateMessage addWarehouseProduct(WarehouseOfProductDTO warehouseOfProductDTO, Integer restaurant);
    StateMessage editeWarehouseProduct(WarehouseOfProductDTO warehouseOfProductDTO);
    StateMessage deleteWarehouseProduct(Integer id);
    WarehouseOfProductDTO getWarehouseProduct(Integer productId);
    List<WarehouseOfProductDTO> getWarehouseProducts(Integer restaurantId);
}
