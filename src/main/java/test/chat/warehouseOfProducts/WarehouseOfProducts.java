package test.chat.warehouseOfProducts;

import com.fasterxml.jackson.annotation.JsonIgnore;
import jakarta.persistence.*;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import test.chat.allClasses.SuperClass;
import test.chat.inoutProduct.InoutProduct;
import test.chat.restaurant.Restaurant;

import java.math.BigDecimal;
import java.util.List;

@Entity
@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
public class WarehouseOfProducts extends SuperClass {
    @Column(unique = true, nullable = false)
    private String name;
    @Column(precision = 19, scale = 6)
    private BigDecimal weight = BigDecimal.valueOf(0);
    @Column(precision = 19, scale = 6)
    private BigDecimal price = BigDecimal.valueOf(0);
    @Column(precision = 19, scale = 6)
    private BigDecimal bodyPrice = BigDecimal.valueOf(0);
    @Column(precision = 19, scale = 6)
    private BigDecimal totalPrice = BigDecimal.valueOf(0);
    private Boolean deleted = false;
    @ManyToOne
    @JsonIgnore
    private Restaurant restaurant;
    @OneToMany(orphanRemoval = true)
    @JoinColumn(name = "warehouse_of_products_id",referencedColumnName = "id")
    private List<InoutProduct> inoutProducts;
    public WarehouseOfProducts(String name,Restaurant restaurant) {
        this.name = name;
        this.restaurant = restaurant;
    }

    public BigDecimal getTotalPrice() {
        return this.bodyPrice.multiply(this.weight);
    }

    public WarehouseOfProducts(String name, BigDecimal weight, BigDecimal price, BigDecimal bodyPrice, BigDecimal totalPrice, Restaurant restaurant) {
        this.name = name;
        this.weight = weight;
        this.price = price;
        this.bodyPrice = bodyPrice;
        this.totalPrice = totalPrice;
        this.restaurant = restaurant;
    }
}
