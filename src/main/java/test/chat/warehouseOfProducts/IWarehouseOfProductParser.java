package test.chat.warehouseOfProducts;

import test.chat.restaurant.Restaurant;

import java.math.BigDecimal;
import java.math.RoundingMode;

public interface IWarehouseOfProductParser {
    default WarehouseOfProducts dtoToProduct(WarehouseOfProductDTO warehouseDTO, Restaurant restaurant){
        return new WarehouseOfProducts(
                warehouseDTO.getName(),
                warehouseDTO.getWeight(),
                warehouseDTO.getPrice(),
                warehouseDTO.getPrice(),
                warehouseDTO.getPrice(),
                restaurant
        );
    }
    default WarehouseOfProductDTO wareHouseProductToDTO(WarehouseOfProducts warehouse){
        return new WarehouseOfProductDTO(
                warehouse.getId(),
                warehouse.getName(),
                warehouse.getWeight(),
                warehouse.getPrice(),
                warehouse.getBodyPrice(),
                warehouse.getTotalPrice()
        );
    }

}
