package test.chat.warehouseOfProducts;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public interface WarehouseOfProductRepo extends JpaRepository<WarehouseOfProducts,Integer> {
    List<WarehouseOfProducts> findAllByRestaurant_IdOrderByName(Integer id);
}
