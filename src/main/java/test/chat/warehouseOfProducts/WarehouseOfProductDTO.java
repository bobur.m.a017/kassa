package test.chat.warehouseOfProducts;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.math.BigDecimal;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class WarehouseOfProductDTO {
    private Integer id;
    private String name;
    private BigDecimal weight;
    private BigDecimal price;
    private BigDecimal bodyPrice;
    private BigDecimal totalPrice;
}
