package test.chat.warehouseOfProducts;

import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import test.chat.message.StateMessage;
import test.chat.restaurant.Restaurant;
import test.chat.restaurant.RestaurantRepo;

import java.util.ArrayList;
import java.util.List;

@Service
@RequiredArgsConstructor
public class WarehouseOfProductServices implements IWarehouseProductServices, IWarehouseOfProductParser {
    private final WarehouseOfProductRepo warehouseOfProductRepo;
    private final RestaurantRepo restaurantRepo;

    @Override
    public StateMessage addWarehouseProduct(WarehouseOfProductDTO warehouseOfProductDTO, Integer restaurantId) {
        Restaurant restaurant = restaurantRepo.findById(restaurantId).orElse(null);
        if (restaurant == null){
            return new StateMessage("Restorant topilmadi", false, HttpStatus.NO_CONTENT.value());
        }
        warehouseOfProductRepo.save(new WarehouseOfProducts(warehouseOfProductDTO.getName(),restaurant));
        return new StateMessage("Qo'shildi", true, 200);
    }

    @Override
    public StateMessage editeWarehouseProduct(WarehouseOfProductDTO warehouseOfProductDTO) {
        if (warehouseOfProductDTO.getId() == null) {
            return new StateMessage("Ma'lumot topilmadi", false, HttpStatus.NO_CONTENT.value());
        }
        WarehouseOfProducts warehouseOfProducts = warehouseOfProductRepo.findById(warehouseOfProductDTO.getId()).orElse(null);
        if (warehouseOfProducts == null) {
            return new StateMessage("Ma'lumot topilmadi", false, HttpStatus.NO_CONTENT.value());
        }
        warehouseOfProducts.setName(warehouseOfProducts.getName());
        warehouseOfProductRepo.save(warehouseOfProducts);
        return new StateMessage("Qo'shildi", true, 200);
    }

    @Override
    public StateMessage deleteWarehouseProduct(Integer id) {
        WarehouseOfProducts warehouseOfProducts = warehouseOfProductRepo.findById(id).orElse(null);
        if (warehouseOfProducts == null) {
            return new StateMessage("Topilmadi", false, HttpStatus.NO_CONTENT.value());
        }
        warehouseOfProducts.setDeleted(true);
        warehouseOfProductRepo.save(warehouseOfProducts);
        return new StateMessage("Topilmadi", true, 200);
    }

    @Override
    public WarehouseOfProductDTO getWarehouseProduct(Integer productId) {
        return null;
    }

    @Override
    public List<WarehouseOfProductDTO> getWarehouseProducts(Integer restaurantId) {
        List<WarehouseOfProductDTO> warehouse = new ArrayList<>();
        for (WarehouseOfProducts warehouseOfProducts : warehouseOfProductRepo.findAllByRestaurant_IdOrderByName(restaurantId)) {
            warehouse.add(wareHouseProductToDTO(warehouseOfProducts));
        }

        return warehouse;
    }
}
