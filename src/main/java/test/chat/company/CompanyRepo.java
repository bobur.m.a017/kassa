package test.chat.company;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import test.chat.category.Category;

import java.util.Optional;

@Repository
public interface CompanyRepo extends JpaRepository<Company,Integer> {
    Optional<Company> findByUser_Id(Integer id);
}
