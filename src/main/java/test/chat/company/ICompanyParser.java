package test.chat.company;
import test.chat.food.FoodDTO;
import test.chat.restaurant.Restaurant;
import test.chat.user.Users;

import java.util.List;

public interface ICompanyParser {
    default Company dtoToCompany(CompanyDTO companyDTO, Users user){
        return new Company(
                companyDTO.getName(),
                null,
                user
        );
    }
    default CompanyDTO companyToDTO(Company company, List<Restaurant> restaurants){
        return new CompanyDTO(
                company.getId(),
                company.getName(),
                restaurants
        );
    }

}
