package test.chat.company;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import test.chat.message.StateMessage;
import test.chat.user.UserRepo;
import test.chat.user.Users;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Service
@RequiredArgsConstructor
public class CompanyServices implements ICompanyServices, ICompanyParser {
    private final CompanyRepo companyRepo;
    private final UserRepo userRepo;

    @Override
    public StateMessage addCompany(CompanyDTO companyDTO, String userName) {
        Users users = userRepo.findByUsername(userName).orElse(null);
        if (users != null) {
            companyRepo.save(dtoToCompany(companyDTO, users));
            return new StateMessage("Qo'shildi", true, 200);
        }
        return new StateMessage("Qo'shilmadi", true, 500);
    }

    @Override
    public StateMessage editeCompany(CompanyDTO companyDTO) {
        Optional<Company> companyOptional = companyRepo.findById(companyDTO.getId());
        Company company;
        if (companyOptional.isPresent()) {
            company = companyOptional.get();
            company.setName(companyDTO.getName());
            companyRepo.save(company);
            return new StateMessage("O'zgartirildi", true, 200);
        }
        return new StateMessage("O'zgartirilmadi", true, 500);
    }

    @Override
    public StateMessage deleteCompany(Integer id) {
        try {
            companyRepo.deleteById(id);
            return new StateMessage("O'chirildi", true, 200);
        } catch (Exception e) {
            return new StateMessage("O'chirilmadi", true, 500);
        }
    }

    @Override
    public CompanyDTO getCompany(Integer companyId) {
        Company company = companyRepo.findById(companyId).orElse(null);
        if (company == null){
            return null;
        }
        return companyToDTO(company,null);

    }

    @Override
    public List<CompanyDTO> getCompanies() {
        List<Company> companies = companyRepo.findAll();
        List<CompanyDTO> companyDTOS = new ArrayList<>();
        for (Company company : companies) {
            companyDTOS.add(companyToDTO(company,null));
        }
        return companyDTOS;
    }

}
