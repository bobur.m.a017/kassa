package test.chat.company;

import test.chat.message.StateMessage;

import java.util.List;

public interface ICompanyServices {
    StateMessage addCompany(CompanyDTO companyDTO,String userName);
    StateMessage editeCompany(CompanyDTO companyDTO);
    StateMessage deleteCompany(Integer id);
    CompanyDTO getCompany(Integer companyId);
    List<CompanyDTO> getCompanies();
}
