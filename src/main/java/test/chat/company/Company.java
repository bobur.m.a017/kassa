package test.chat.company;

import com.fasterxml.jackson.annotation.JsonIgnore;
import jakarta.persistence.*;
import lombok.*;
import test.chat.allClasses.SuperClass;
import test.chat.restaurant.Restaurant;
import test.chat.user.Users;

import java.util.List;

@Entity
@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
public class Company extends SuperClass {
    private String name;
    @OneToMany(cascade = CascadeType.ALL)
    @JoinColumn(name = "company_id", referencedColumnName = "id")
    private List<Restaurant> restaurants;

    @OneToOne(cascade = CascadeType.ALL,optional = false)
    @JsonIgnore
    private Users user;

}
