package test.chat.delivery;

import com.fasterxml.jackson.annotation.JsonIgnore;
import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.ManyToOne;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import test.chat.allClasses.SuperClass;
import test.chat.restaurant.Restaurant;

@Entity
@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
public class Delivery extends SuperClass {
    private String name;
    @Column(unique = true)
    private String phoneNumber;
    @ManyToOne
    @JsonIgnore
    private Restaurant restaurant;
}
