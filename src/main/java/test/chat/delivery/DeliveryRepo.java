package test.chat.delivery;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface DeliveryRepo extends JpaRepository<Delivery,Integer> {
    List<Delivery> findAllByRestaurant_Id(Integer restaurantId);
}
