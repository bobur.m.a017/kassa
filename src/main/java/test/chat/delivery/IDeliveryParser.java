package test.chat.delivery;

import test.chat.delivery.Delivery;
import test.chat.food.FoodDTO;

import java.util.List;

public interface IDeliveryParser {
    default Delivery dtoToDelivery(DeliveryDTO categoryDTO){
        return new Delivery(
                categoryDTO.getName(),
                categoryDTO.getPhoneNumber(),
                null
        );
    }
    default DeliveryDTO deliveryToDTO(Delivery category){
        if (category == null){
            return null;
        }
        return new DeliveryDTO(
                category.getId(),
                category.getName(),
                category.getPhoneNumber()
        );
    }

}
