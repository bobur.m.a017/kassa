package test.chat.delivery;

import test.chat.message.StateMessage;

import java.util.List;

public interface IDeliveryService {
    StateMessage addDelivery(DeliveryDTO deliveryDTO,Integer restaurantId);
    StateMessage editeDelivery(DeliveryDTO deliveryDTO);
    StateMessage deleteDelivery(Integer id);
    StateMessage relationsDelivery(Integer id,Integer deliveryDTO);
    List<DeliveryDTO> getDeliveries(Integer restaurantId);
}
