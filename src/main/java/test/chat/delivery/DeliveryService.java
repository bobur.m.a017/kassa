package test.chat.delivery;

import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import test.chat.message.StateMessage;
import test.chat.order.OrderRepo;
import test.chat.order.Orders;
import test.chat.restaurant.Restaurant;
import test.chat.restaurant.RestaurantRepo;

import java.util.ArrayList;
import java.util.List;

@Service
@RequiredArgsConstructor
public class DeliveryService implements IDeliveryService {
    private final DeliveryRepo deliveryRepo;
    private final OrderRepo orderRepo;
    private final RestaurantRepo restaurantRepo;

    @Override
    public StateMessage addDelivery(DeliveryDTO deliveryDTO, Integer restaurantId) {
        Restaurant restaurant = restaurantRepo.findById(restaurantId).orElse(null);
        if (restaurant == null) {
            return new StateMessage("Restaurant id yuq", false, HttpStatus.NO_CONTENT.value());
        }
        deliveryRepo.save(new Delivery(deliveryDTO.getName(), deliveryDTO.getPhoneNumber(), restaurant));
        return new StateMessage("Qoi'shildi", true, 200);
    }

    @Transactional
    @Override
    public StateMessage editeDelivery(DeliveryDTO deliveryDTO) {
        Delivery delivery = deliveryRepo.findById(deliveryDTO.getId()).orElse(null);
        if (delivery == null) {
            return new StateMessage("Yetqazuvchi yuq", false, HttpStatus.NO_CONTENT.value());
        }
        delivery.setName(deliveryDTO.getName());
        delivery.setPhoneNumber(deliveryDTO.getPhoneNumber());
        deliveryRepo.save(delivery);
        return new StateMessage("O'zgartirildi", true, 200);
    }

    @Override
    @Transactional
    public StateMessage deleteDelivery(Integer id) {
        deliveryRepo.deleteById(id);
        return new StateMessage("O'chirildi", true, 200);
    }

    @Override
    public StateMessage relationsDelivery(Integer id, Integer deliveryDTO) {
        Orders orders = orderRepo.findById(id).orElse(null);
        if (orders == null){
            return new StateMessage("Buyurtma yuq", false, HttpStatus.NO_CONTENT.value());
        }
        Delivery delivery = deliveryRepo.findById(deliveryDTO).orElse(null);
        if (delivery == null){
            return new StateMessage("Buyurtmachi yuq", false, HttpStatus.NO_CONTENT.value());
        }
        orders.setDelivery(delivery);
        orderRepo.save(orders);
        return new StateMessage("O'zgartirildi", true, 200);
    }

    @Override
    public List<DeliveryDTO> getDeliveries(Integer restaurantId) {
        System.out.println(restaurantId);
        List<Delivery> allByRestaurantId = deliveryRepo.findAllByRestaurant_Id(restaurantId);
        if (allByRestaurantId.isEmpty()){
            return new ArrayList<>();
        }
        return allByRestaurantId.stream().map(delivery -> new DeliveryDTO(delivery.getId(), delivery.getName(), delivery.getPhoneNumber())).toList();
    }

}
