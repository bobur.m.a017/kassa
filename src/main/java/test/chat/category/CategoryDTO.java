package test.chat.category;
import jakarta.annotation.Nullable;
import lombok.*;
import test.chat.food.FoodDTO;
import java.util.List;
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class CategoryDTO {
    private Integer id;
    private String name;
    private List<FoodDTO> foods;

    public CategoryDTO(String name, List<FoodDTO> foods) {
        this.name = name;
        this.foods = foods;
    }
}
