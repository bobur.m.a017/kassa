package test.chat.category;

import jakarta.persistence.*;
import lombok.*;
import test.chat.allClasses.SuperClass;
import test.chat.food.Food;

import java.sql.Timestamp;
import java.util.List;

@Entity
@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
public class Category extends SuperClass {
    private String name;
    @OneToMany
    @JoinColumn(name = "category_id",referencedColumnName = "id")
    private List<Food> food;

    public Category(String name) {
        this.name = name;
    }
}
