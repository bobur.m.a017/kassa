package test.chat.category;

import test.chat.food.FoodDTO;
import test.chat.message.StateMessage;

import java.util.List;

public interface ICategoryParser {
    default Category dtoToCategory(CategoryDTO categoryDTO){
        return new Category(
                categoryDTO.getName()
        );
    }
    default CategoryDTO categoryToDTO(Category category, List<FoodDTO> foodList){
        return new CategoryDTO(
                category.getId(),
                category.getName(),
                foodList
        );
    }

}
