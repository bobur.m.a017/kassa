package test.chat.category;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import test.chat.food.Food;
import test.chat.food.FoodDTO;
import test.chat.food.IFoodParser;
import test.chat.message.StateMessage;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Service
@RequiredArgsConstructor
public class CategoryServices implements ICategoryServices, ICategoryParser, IFoodParser {
    private final CategoryRepo categoryRepo;

    @Override
    public StateMessage addCategory(CategoryDTO categoryDTO) {
        if (categoryDTO.getName() != null) {
            categoryRepo.save(new Category(categoryDTO.getName()));
        return new StateMessage("Qo'shildi", true, 200);
        }
        return new StateMessage("Qo'shilmadi", true, 500);
    }

    @Override
    public StateMessage editeCategory(CategoryDTO categoryDTO) {
        Optional<Category> categoryOptional = categoryRepo.findById(categoryDTO.getId());
        Category category;
        if (categoryOptional.isPresent()) {
            category = categoryOptional.get();
            category.setName(categoryDTO.getName());
            categoryRepo.save(category);
            return new StateMessage("Qo'shildi", true, 200);
        }
        return new StateMessage("Qo'shilmadi", true, 500);
    }

    @Override
    public StateMessage deleteCategory(Integer id) {
        try {
            categoryRepo.deleteById(id);
            return new StateMessage("O'chirildi", true, 200);
        } catch (Exception e) {
            return new StateMessage("O'chirilmadi", true, 500);
        }
    }

    @Override
    public CategoryDTO getCategory(Integer categoryId) {
        Optional<Category> optionalCategory = categoryRepo.findById(categoryId);
        if (optionalCategory.isPresent()) {
            List<FoodDTO> foodDTOList = new ArrayList<>();
            Category category = optionalCategory.get();
            if (category.getFood() != null) {
                for (Food food : optionalCategory.get().getFood()) {
                    foodDTOList.add(foodToDTO(food,null));
                }
                return categoryToDTO(optionalCategory.get(), foodDTOList);
            }
        }
        return null;
    }

    @Override
    public List<CategoryDTO> getCategories() {
        List<Category> categories = categoryRepo.findAll();
        List<CategoryDTO> categoryDTOS = new ArrayList<>();
        for (Category category : categories) {
            List<FoodDTO> foodDTOList = new ArrayList<>();
            if (category.getFood() != null) {
                for (Food food : category.getFood()) {
                    foodDTOList.add(foodToDTO(food,null));
                }
            }
            categoryDTOS.add(categoryToDTO(category, foodDTOList));
        }
        return categoryDTOS;
    }

}
