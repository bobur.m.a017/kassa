package test.chat.category;

import test.chat.message.StateMessage;

import java.util.List;

public interface ICategoryServices {
    StateMessage addCategory(CategoryDTO categoryDTO);
    StateMessage editeCategory(CategoryDTO categoryDTO);
    StateMessage deleteCategory(Integer id);
    CategoryDTO getCategory(Integer categoryId);
    List<CategoryDTO> getCategories();
}
