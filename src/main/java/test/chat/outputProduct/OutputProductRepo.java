package test.chat.outputProduct;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;
import test.chat.workDay.WorkDay;

import java.sql.Timestamp;
import java.util.List;

@Repository
public interface OutputProductRepo extends JpaRepository<OutputProduct, Integer> {

    @Query("SELECT o FROM OutputProduct o WHERE o.restaurant.id = ?1 AND ( cast(?2 as timestamp ) IS NULL OR o.updatedDate > ?2 )AND (cast(?3 as timestamp ) IS NULL OR o.updatedDate < ?3) order by o.updatedDate")
    List<OutputProduct> findAllByOutputProductByRestaurantAndDate(Integer restaurantId, Timestamp startDate, Timestamp endDate);
    List<OutputProduct> findAllByWorkDayIn(List<WorkDay> workDays);
    List<OutputProduct> findAllByWorkDay_Id(Integer id);
    @Override
    @Modifying
    void deleteById(Integer id);
    OutputProduct findByProductCompositionSave_Id(Integer id);
}
