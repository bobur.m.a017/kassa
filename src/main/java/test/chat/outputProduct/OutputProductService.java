package test.chat.outputProduct;

import lombok.RequiredArgsConstructor;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import test.chat.inoutProduct.*;
import test.chat.message.StateMessage;
import test.chat.restaurant.Restaurant;
import test.chat.restaurant.RestaurantRepo;
import test.chat.statics.StaticMethods;
import test.chat.statusWork.StatusWorkRepo;
import test.chat.user.UserRepo;
import test.chat.user.Users;
import test.chat.warehouseOfProducts.IWarehouseOfProductParser;
import test.chat.warehouseOfProducts.WarehouseOfProductRepo;
import test.chat.warehouseOfProducts.WarehouseOfProducts;
import test.chat.workDay.DayWareHouseOfProducts;
import test.chat.workDay.WorkDay;
import test.chat.workDay.WorkDayRepo;
import test.chat.workDay.WorkDayWareHoseOfProductRepo;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.sql.Timestamp;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

@Service
@RequiredArgsConstructor
@EnableScheduling
public class OutputProductService implements IOutputProductService, IOutputProductParser, IWarehouseOfProductParser {
    private final InoutProductRepo inoutProductRepo;
    private final OutputProductRepo outputProductRepo;
    private final WorkDayRepo workDayRepo;
    private final StatusWorkRepo statusWorkRepo;
    private final RestaurantRepo restaurantRepo;
    private final WarehouseOfProductRepo warehouseOfProductRepo;
    private final WorkDayWareHoseOfProductRepo workDayWareHoseOfProductRepo;

    @Transactional
    @Override
    public List<OutputProductDTO> getOutputProducts(Integer restaurantId, Long startDate, Long endDate) {
        Timestamp start = StaticMethods.getStartCurrentDate();
        if (startDate != null) {
            start = new Timestamp(startDate);
        }
        Timestamp end = StaticMethods.addDayToMillisecond(1);
        if (endDate != null) {
            end = new Timestamp(endDate);
        }
        List<OutputProduct> allByOutputProductByRestaurantAndDate = outputProductRepo.findAllByOutputProductByRestaurantAndDate(restaurantId, start,end);

        List<OutputProductDTO> outputProductDTOList = new ArrayList<>();
        for (OutputProduct outputProduct : allByOutputProductByRestaurantAndDate) {
            outputProductDTOList.add(outProductToDTO(outputProduct, wareHouseProductToDTO(outputProduct.getWarehouseOfProducts())));
        }
        return outputProductDTOList;
    }
}
