package test.chat.outputProduct;

import com.fasterxml.jackson.annotation.JsonIgnore;
import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.ManyToOne;
import jakarta.persistence.OneToOne;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import test.chat.allClasses.SuperClass;
import test.chat.productComposFromSave.ProductCompositionSave;
import test.chat.restaurant.Restaurant;
import test.chat.warehouseOfProducts.WarehouseOfProducts;
import test.chat.workDay.WorkDay;

import java.math.BigDecimal;

@Entity
@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
public class OutputProduct extends SuperClass {
    private String name;
    @Column(precision = 19, scale = 6)
    private BigDecimal weight;
    @Column(precision = 19, scale = 6)
    private BigDecimal totalWeight;
    @Column(precision = 19, scale = 6)
    private BigDecimal price;
    @Column(precision = 19, scale = 6)
    private BigDecimal totalPrice;

    @ManyToOne(optional = false)
    @JsonIgnore
    private Restaurant restaurant;

    @OneToOne(optional = false)
    @JsonIgnore
    private ProductCompositionSave productCompositionSave;

    @ManyToOne(optional = false)
    @JsonIgnore
    private WarehouseOfProducts warehouseOfProducts;

    @ManyToOne(optional = false)
    @JsonIgnore
    private WorkDay workDay;

    public OutputProduct(String name, BigDecimal weight, BigDecimal totalWeight, BigDecimal price, BigDecimal totalPrice, Restaurant restaurant, WarehouseOfProducts warehouseOfProducts) {
        this.name = name;
        this.weight = weight;
        this.totalWeight = totalWeight;
        this.price = price;
        this.totalPrice = totalPrice;
        this.restaurant = restaurant;
        this.warehouseOfProducts = warehouseOfProducts;
    }

}
