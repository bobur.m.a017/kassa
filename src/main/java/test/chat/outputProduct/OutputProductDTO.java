package test.chat.outputProduct;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import test.chat.warehouseOfProducts.WarehouseOfProductDTO;

import java.math.BigDecimal;
import java.sql.Timestamp;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class OutputProductDTO {
    private Integer id;
    private String name;
    private BigDecimal weight;
    private BigDecimal price;
    private BigDecimal totalPrice;
    private BigDecimal totalWeight;
    private WarehouseOfProductDTO productDTO;
    private Timestamp createdDate;
}
