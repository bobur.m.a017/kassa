package test.chat.outputProduct;
import test.chat.restaurant.Restaurant;
import test.chat.warehouseOfProducts.WarehouseOfProductDTO;
import test.chat.warehouseOfProducts.WarehouseOfProducts;

import java.math.BigDecimal;
import java.math.RoundingMode;

public interface IOutputProductParser {
    default OutputProduct dtoToOutputProduct(OutputProductDTO inoutProductDTO, BigDecimal totalWeight, Restaurant restaurant, WarehouseOfProducts warehouse){
        return new OutputProduct(
                inoutProductDTO.getName(),
                new BigDecimal(inoutProductDTO.getWeight().toString()).setScale(6, RoundingMode.HALF_EVEN),
                totalWeight,
                new BigDecimal(inoutProductDTO.getPrice().toString()).setScale(6, RoundingMode.HALF_EVEN),
                new BigDecimal(inoutProductDTO.getTotalPrice().toString()).setScale(6, RoundingMode.HALF_EVEN),
                restaurant,
                warehouse
        );
    }
    default OutputProductDTO outProductToDTO(OutputProduct inoutProduct, WarehouseOfProductDTO productDTO){
        return new OutputProductDTO(
                inoutProduct.getId(),
                inoutProduct.getName(),
                inoutProduct.getWeight(),
                inoutProduct.getPrice(),
                inoutProduct.getTotalPrice(),
                inoutProduct.getTotalWeight(),
                productDTO,
                inoutProduct.getUpdatedDate()
        );
    }

}
