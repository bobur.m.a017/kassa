package test.chat.outputProduct;

import java.util.List;

public interface IOutputProductService {
    List<OutputProductDTO> getOutputProducts(Integer restaurantId,Long start,Long end);
}
