package test.chat.productComposFromSave;

import jakarta.validation.constraints.NotNull;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import test.chat.warehouseOfProducts.WarehouseOfProductDTO;
import test.chat.warehouseOfProducts.WarehouseOfProducts;

import java.math.BigDecimal;
import java.sql.Timestamp;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class ProductCompositionSaveDTO {
    private Integer id;
    private BigDecimal weight;
    private BigDecimal price;
    private BigDecimal totalPrice;
    private Timestamp createdDate;
    @NotNull
    private WarehouseOfProductDTO productDTO;
}
