package test.chat.productComposFromSave;
import test.chat.productComposition.ProductComposition;
import test.chat.productComposition.ProductCompositionDTO;
import test.chat.restaurant.StatusTypes;
import test.chat.statusWork.StatusWork;
import test.chat.warehouseOfProducts.WarehouseOfProductDTO;
import test.chat.workDay.WorkDay;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

public interface IProductCompositionFromSaveParser {
    default ProductCompositionSave dtoToProductCompositionSave(ProductComposition product, String status, WorkDay workDay,Integer count){
        return new ProductCompositionSave(
                product.getWarehouseOfProducts().getName(),
                product.getWeight().multiply(new BigDecimal(count).setScale(6)),
                product.getWarehouseOfProducts().getPrice(),
                product.getWarehouseOfProducts().getBodyPrice().multiply(product.getWeight()),
                product.getWarehouseOfProducts(),
                status,
                workDay,
                product.getWarehouseOfProducts().getBodyPrice().multiply(product.getWeight())
        );
    }
    default List<ProductCompositionSave> dtoToProductCompositionSaveList(List<ProductComposition> products,String status,WorkDay workDay,Integer count){
        List<ProductCompositionSave>productCompositionSaves = new ArrayList<>();
        for (ProductComposition product : products) {
            productCompositionSaves.add(
                    new ProductCompositionSave(
                            product.getWarehouseOfProducts().getName(),
                            product.getWeight().multiply(new BigDecimal(count).setScale(6)),
                            product.getWarehouseOfProducts().getPrice(),
                            product.getWarehouseOfProducts().getBodyPrice().multiply(product.getWeight()),
                            product.getWarehouseOfProducts(),
                            status,
                            workDay,
                            product.getWarehouseOfProducts().getBodyPrice().multiply(product.getWeight())
                    )
            );
        }
        return productCompositionSaves;
    }
    default ProductCompositionDTO productCompSaveToDTO(ProductCompositionSave productCompositions, WarehouseOfProductDTO warehouse){
        return new ProductCompositionDTO(
                productCompositions.getId(),
                productCompositions.getWeight(),
                productCompositions.getWeight(),
                null,
                warehouse
        );
    }
    default List<ProductCompositionDTO> productCompSaveToDTOList(List<ProductComposition> productCompositions){
        List<ProductCompositionDTO> compositionDTOS = new ArrayList<>();
        for (ProductComposition productComposition : productCompositions) {
//            compositionDTOS.add(productCompToDTO(productComposition,new ProductDTO(productComposition.getWarehouseOfProducts().getProduct().getId(),productComposition.getWarehouseOfProducts().getProduct().getName())));
        }
        return compositionDTOS;
    }

}
