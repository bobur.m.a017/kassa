package test.chat.productComposFromSave;

import com.fasterxml.jackson.annotation.JsonIgnore;
import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.ManyToOne;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import test.chat.allClasses.SuperClass;
import test.chat.foodFromSave.FoodFromSave;
import test.chat.warehouseOfProducts.WarehouseOfProducts;
import test.chat.workDay.WorkDay;

import java.math.BigDecimal;

@Entity
@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
public class ProductCompositionSave extends SuperClass {

    @Column(nullable = false)
    private String name;

    @Column(nullable = false,precision = 19, scale = 6)
    private BigDecimal weight;
    @Column(precision = 19, scale = 6)
    private BigDecimal price = BigDecimal.valueOf(0);
    @Column(precision = 19, scale = 6)
    private BigDecimal totalPrice = BigDecimal.valueOf(0);
    @Column(precision = 19, scale = 6)
    private BigDecimal bodyPrice = BigDecimal.valueOf(0);

    @ManyToOne
    @JsonIgnore
    private FoodFromSave foodFromSave;

    @ManyToOne(optional = false)
    @JsonIgnore
    private WarehouseOfProducts warehouseOfProducts;
    @ManyToOne(optional = false)
    @JsonIgnore
    private WorkDay workDay;
    private String type;
    public ProductCompositionSave(String name, BigDecimal weight, BigDecimal price, BigDecimal totalPrice, WarehouseOfProducts warehouseOfProducts,String type,WorkDay workDay,BigDecimal bodyPrice) {
        this.name = name;
        this.weight = weight;
        this.price = price;
        this.totalPrice = totalPrice;
        this.warehouseOfProducts = warehouseOfProducts;
        this.type = type;
        this.workDay = workDay;
        this.bodyPrice = bodyPrice;
    }
}