package test.chat.productComposFromSave;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;
import test.chat.order.Orders;
import test.chat.workDay.WorkDay;

import java.sql.Timestamp;
import java.util.List;

@Repository
public interface ProductComposeSaveRepo extends JpaRepository<ProductCompositionSave,Integer> {
    List<ProductCompositionSave>findAllByWorkDayIn(List<WorkDay> workDays);
    List<ProductCompositionSave>findAllByWorkDay_Id(Integer workDayId);
//    @Query("SELECT o FROM Orders o WHERE o.updatedDate > ?1 AND o.updatedDate < ?2 AND  o.restaurant.id = ?3 AND (o.type = ?4 OR ?4 is null)")
//    List<Orders> findAllByUpdatedDateQueryNative(Timestamp startDate, Timestamp endDate,Integer restaurantId,String orderType);
}
