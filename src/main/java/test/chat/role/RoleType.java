package test.chat.role;

import lombok.Getter;

@Getter

public enum RoleType {

    SUPER_ADMIN(1, "ROLE_SUPER_ADMIN","Super admin"),
    ROLE_ADMIN(2, "ROLE_ADMIN","Restoran admin"),
    ROLE_DELIVERY_ADMIN(3, "ROLE_DELIVERY_ADMIN","Yetkazish xizmati admini "),
    ROLE_USER(4, "ROLE_USER","G'aznachi"),
    ROLE_DIRECTOR(4, "ROLE_DIRECTOR","Rahbar");

    private final Integer id;
    private final String name;
    private final String roleName;

    RoleType(int id, String name,String roleName) {
        this.id = id;
        this.name = name;
        this.roleName = roleName;
    }


}
