package test.chat.role;

import jakarta.persistence.*;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.springframework.security.core.GrantedAuthority;
import test.chat.allClasses.SuperClass;

@Entity
@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
public class Role extends SuperClass implements GrantedAuthority {
    private String name;
    private String roleName;

    @Override
    public String getAuthority() {
        return name;
    }
}
