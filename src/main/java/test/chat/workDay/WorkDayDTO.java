package test.chat.workDay;

import lombok.Getter;
import lombok.Setter;
import test.chat.inoutProduct.InoutProductDTO;
import test.chat.outputProduct.OutputProductDTO;
import test.chat.productComposFromSave.ProductCompositionSaveDTO;

import java.math.BigDecimal;
import java.sql.Timestamp;
import java.util.List;
@Setter
@Getter
public class WorkDayDTO {
    private Integer id;
    private Integer year;
    private Integer month;
    private Integer day;
    private Integer orderCount;
    private Timestamp createdDate;
    private BigDecimal orderAllPrice;
    private BigDecimal orderAllBodyPrice;
    private BigDecimal inoutAllPrice;
    private BigDecimal outputAllPrice;
    private List<OutputProductDTO> outputProductDTOS;
    private List<DayWareHouseOfProductsDTO> dayWareHouseOfProductsDTOS;
    private List<InoutProductDTO> inoutProductDTOS;
    private List<ProductCompositionSaveDTO> productCompositionSaveDTOS;
}
