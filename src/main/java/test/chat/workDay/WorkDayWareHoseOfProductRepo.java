package test.chat.workDay;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public interface WorkDayWareHoseOfProductRepo extends JpaRepository<DayWareHouseOfProducts,Integer> {

}
