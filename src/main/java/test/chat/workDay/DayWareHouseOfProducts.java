package test.chat.workDay;
import jakarta.persistence.Entity;
import jakarta.persistence.ManyToOne;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import test.chat.allClasses.SuperClass;
import test.chat.warehouseOfProducts.WarehouseOfProducts;

import java.math.BigDecimal;

@Entity
@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
public class DayWareHouseOfProducts extends SuperClass {
    private String name;
    private BigDecimal weight;
    private BigDecimal price = BigDecimal.valueOf(0);
    private BigDecimal totalPrice = BigDecimal.valueOf(0);
    @ManyToOne
    private WorkDay workDay;
    @ManyToOne
    private WarehouseOfProducts warehouseOfProducts;
}
