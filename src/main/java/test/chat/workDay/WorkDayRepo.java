package test.chat.workDay;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import test.chat.warehouseOfProducts.WarehouseOfProducts;

import java.sql.Timestamp;
import java.util.List;
import java.util.Optional;

@Repository
public interface WorkDayRepo extends JpaRepository<WorkDay,Integer> {
    Optional<WorkDay> findByRestaurant_IdAndYearAndMonthAndDay(Integer id,Integer year,Integer month,Integer day);
    List<WorkDay> findAllByYearAndMonthAndDay(Integer year,Integer  month,Integer day);

    @Query("SELECT o FROM WorkDay o WHERE  ( o.createdDate > :startDate or cast(:startDate as timestamp ) is null ) AND ( o.createdDate < :endDate or cast(:endDate as timestamp ) is null) AND  o.restaurant.id = :restaurantId")
    List<WorkDay> findAllByCreatedDate(@Param("startDate") Timestamp startDate, @Param("endDate") Timestamp endDate, @Param("restaurantId") Integer restaurantId);
}
