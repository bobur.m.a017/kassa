package test.chat.workDay;


import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.math.BigDecimal;

@Setter
@Getter
@AllArgsConstructor
@NoArgsConstructor
public class DayWareHouseOfProductsDTO{
    private String name;
    private BigDecimal weight;
    private BigDecimal price = BigDecimal.valueOf(0);
    private BigDecimal totalPrice = BigDecimal.valueOf(0);

}
