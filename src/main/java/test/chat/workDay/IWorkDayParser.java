package test.chat.workDay;

import test.chat.inoutProduct.InoutProductDTO;
import test.chat.outputProduct.OutputProductDTO;

import java.util.List;

public interface IWorkDayParser {
    default WorkDay dtoToWorkDay(WorkDayDTO workDayDTO){
        return null;
    }
    default WorkDayDTO workDayToDto(
                                    WorkDay workDay,
                                    List<DayWareHouseOfProductsDTO> dayWareHouseOfProductsDTO,
                                    List<InoutProductDTO> inoutProductDTO,
                                    List<OutputProductDTO> outputProductDTO
                                    ){
        return new WorkDayDTO(

        );
    }
}
