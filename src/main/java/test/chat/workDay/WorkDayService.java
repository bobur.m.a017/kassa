package test.chat.workDay;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import test.chat.inoutProduct.InoutProduct;
import test.chat.inoutProduct.InoutProductDTO;
import test.chat.inoutProduct.InoutProductRepo;
import test.chat.order.OrderRepo;
import test.chat.order.Orders;
import test.chat.order.StatusOrder;
import test.chat.order.TypeOrder;
import test.chat.outputProduct.OutputProduct;
import test.chat.outputProduct.OutputProductDTO;
import test.chat.outputProduct.OutputProductRepo;
import test.chat.productComposFromSave.ProductComposeSaveRepo;
import test.chat.productComposFromSave.ProductCompositionSave;
import test.chat.productComposFromSave.ProductCompositionSaveDTO;
import test.chat.warehouseOfProducts.IWarehouseOfProductParser;
import test.chat.warehouseOfProducts.WarehouseOfProductRepo;
import test.chat.warehouseOfProducts.WarehouseOfProducts;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;

@RequiredArgsConstructor
@Service
public class WorkDayService implements IWorkDayService, IWorkDayParser, IWarehouseOfProductParser {
    private final WorkDayRepo workDayRepo;
    private final DayWarehouseOfProductRepo dayWarehouseOfProductRepo;
    private final OutputProductRepo outputProductRepo;
    private final InoutProductRepo inoutProductRepo;
    private final ProductComposeSaveRepo productComposeSaveRepo;
    private final OrderRepo orderRepo;
    private final WarehouseOfProductRepo warehouseOfProductRepo;

    @Override
    public WorkDayDTO getWorkDay(Long time) {
        return null;
    }

    @Override
    public WorkDayDTO getWorkDay(Long start, Long end, Integer restaurantId) {
        List<WorkDay> allWorkdayByDate = workDayRepo.findAllByCreatedDate(new Timestamp(start), new Timestamp(end), restaurantId);
        List<WarehouseOfProducts> warehouseOfProductsList = warehouseOfProductRepo.findAllByRestaurant_IdOrderByName(restaurantId);
        WorkDayDTO workDayDTO = getOrderStatistics(getOrderByWorkdays(allWorkdayByDate));
        workDayDTO = getByInoutProduct(getInoutProductByWorkDays(allWorkdayByDate), warehouseOfProductsList, workDayDTO);
        workDayDTO = getByProductCompositions(getProductCompositionSaveByWorkDays(allWorkdayByDate),  warehouseOfProductsList, workDayDTO);
        workDayDTO = getByOutputProduct(getOutputsByWorkDays(allWorkdayByDate), warehouseOfProductsList, workDayDTO);
        return workDayDTO;
    }

    @Override
    public List<WorkDayDTO> getWorkDays(Long start, Long end, Integer restaurantId) {
        List<WorkDay> allWorkdayByDate = workDayRepo.findAllByCreatedDate(new Timestamp(start), new Timestamp(end), restaurantId);
        List<WarehouseOfProducts> warehouseOfProductsList = warehouseOfProductRepo.findAllByRestaurant_IdOrderByName(restaurantId);
        List<WorkDayDTO> workDayDTOList = new ArrayList<>();
        for (WorkDay workDay : allWorkdayByDate) {
            WorkDayDTO workDayDTO = getOrderStatistics(getOrderByWorkday(workDay));
            workDayDTO = getByInoutProduct(getInoutProductByWorkDay(workDay), warehouseOfProductsList, workDayDTO);
            workDayDTO = getByOutputProduct(getOutputsByWorkDay(workDay), warehouseOfProductsList, workDayDTO);
            workDayDTO = getByProductCompositions(getProductCompositionSaveByWorkDay(workDay), warehouseOfProductsList, workDayDTO);
            workDayDTO = getByDayWarehouse(getDayWarehouseByWorkDay(workDay),workDayDTO);
            workDayDTO.setCreatedDate(workDay.getCreatedDate());
            workDayDTO.setId(workDay.getId());
            workDayDTO.setDay(workDayDTO.getDay());
            workDayDTO.setMonth(workDayDTO.getMonth());
            workDayDTO.setYear(workDayDTO.getYear());
            workDayDTOList.add(workDayDTO);
        }
        return workDayDTOList;
    }

    public List<Orders> getOrderByWorkdays(List<WorkDay> workDays) {
        return orderRepo.findAllByWorkDayInAndStatus(workDays, StatusOrder.PAID.getName());
    }
    public List<DayWareHouseOfProducts> getDayWarehouseByWorkDay(WorkDay workDay){
        return dayWarehouseOfProductRepo.findAllByWorkDay(workDay);
    }

    public List<Orders> getOrderByWorkday(WorkDay workDay) {
        return orderRepo.findAllByWorkDay_IdAndStatus(workDay.getId(), StatusOrder.PAID.getName());
    }

    public List<OutputProduct> getOutputsByWorkDays(List<WorkDay> workDays) {
        return outputProductRepo.findAllByWorkDayIn(workDays);
    }
    public List<ProductCompositionSave> getProductCompositionSaveByWorkDays(List<WorkDay> workDays) {
        return productComposeSaveRepo.findAllByWorkDayIn(workDays);
    }
    public List<ProductCompositionSave> getProductCompositionSaveByWorkDay(WorkDay workDay) {
        return productComposeSaveRepo.findAllByWorkDay_Id(workDay.getId());
    }

    public List<OutputProduct> getOutputsByWorkDay(WorkDay workDay) {
        return outputProductRepo.findAllByWorkDay_Id(workDay.getId());
    }

    public List<InoutProduct> getInoutProductByWorkDays(List<WorkDay> workDays) {
        return inoutProductRepo.findAllByWorkDayIn(workDays);
    }

    public List<InoutProduct> getInoutProductByWorkDay(WorkDay workDay) {
        return inoutProductRepo.findAllByWorkDay_Id(workDay.getId());
    }

    public WorkDayDTO getOrderStatistics(List<Orders> ordersList) {
        Integer countOfOrders = ordersList.size();
        BigDecimal ordersBodyPrice = ordersList.stream().map(Orders::getBodyPrice).reduce(BigDecimal.ZERO, BigDecimal::add);
        BigDecimal ordersPrice = ordersList.stream().map(Orders::getPrices).reduce(BigDecimal.ZERO, BigDecimal::add);
        WorkDayDTO workDayDTO = new WorkDayDTO();
        workDayDTO.setOrderAllPrice(ordersPrice);
        workDayDTO.setOrderAllBodyPrice(ordersBodyPrice);
        workDayDTO.setOrderCount(countOfOrders);
        return workDayDTO;
    }
    public WorkDayDTO getByDayWarehouse(List<DayWareHouseOfProducts> wareHouseOfProducts,WorkDayDTO workDayDTO){
        List<DayWareHouseOfProductsDTO> wareHouseOfProductsDTOList = new ArrayList<>();
        for (DayWareHouseOfProducts wareHouseOfProduct : wareHouseOfProducts) {
            DayWareHouseOfProductsDTO ofProducts = new DayWareHouseOfProductsDTO();
            ofProducts.setPrice(wareHouseOfProduct.getPrice());
            ofProducts.setName(wareHouseOfProduct.getName());
            ofProducts.setWeight(wareHouseOfProduct.getWeight());
            ofProducts.setWeight(wareHouseOfProduct.getWeight());
            ofProducts.setTotalPrice(wareHouseOfProduct.getTotalPrice());
            wareHouseOfProductsDTOList.add(ofProducts);
        }
        workDayDTO.setDayWareHouseOfProductsDTOS(wareHouseOfProductsDTOList);
        return workDayDTO;
    }


    public WorkDayDTO getByProductCompositions(List<ProductCompositionSave> productCompositionsSave, List<WarehouseOfProducts> warehouseOfProductsList, WorkDayDTO workDayDTO) {
        List<ProductCompositionSaveDTO> productCompositionSaveDTOS = new ArrayList<>();
        List<WarehouseOfProducts> warehouseOfProductse = new ArrayList<>(warehouseOfProductsList);
        for (WarehouseOfProducts warehouseOfProducts : warehouseOfProductse) {
            for (int i = 0; i < productCompositionsSave.size(); i++) {
                if (productCompositionsSave.get(i).getWarehouseOfProducts().getId().equals(warehouseOfProducts.getId())) {
                    ProductCompositionSaveDTO productCompositionSaveDTO = productCompositionHasOrNo(productCompositionSaveDTOS, warehouseOfProducts.getId());
                    productCompositionSaveDTO.setProductDTO(wareHouseProductToDTO(warehouseOfProducts));
                    productCompositionSaveDTO.setPrice(
                            calculateBodyPrice(checkNullOther(productCompositionsSave.get(i).getTotalPrice()),
                                    productCompositionsSave.get(i).getWeight(),
                                    checkNullOther(checkNullOther(productCompositionSaveDTO.getPrice()).multiply(checkNullOther(productCompositionSaveDTO.getWeight()))),
                                    checkNullOther(productCompositionSaveDTO.getWeight()))
                    );
                    productCompositionSaveDTO.setTotalPrice(checkNullOther(checkNullOther(productCompositionSaveDTO.getPrice()).multiply(checkNullOther(productCompositionSaveDTO.getWeight()))).setScale(2,RoundingMode.HALF_EVEN).add(productCompositionsSave.get(i).getTotalPrice()));
                    productCompositionSaveDTO.setWeight(checkNullOther(productCompositionSaveDTO.getWeight()).add(productCompositionsSave.get(i).getWeight()));
                    productCompositionSaveDTO.setCreatedDate(productCompositionsSave.get(i).getCreatedDate());
                    productCompositionSaveDTO.setId(productCompositionsSave.get(i).getId());
                    workDayDTO.setInoutAllPrice(checkNullOther(workDayDTO.getInoutAllPrice()).add(productCompositionsSave.get(i).getTotalPrice()));
                    if (!productCompositionSaveDTOS.contains(productCompositionSaveDTO)) {
                        productCompositionSaveDTOS.add(productCompositionSaveDTO);
                    }
                    productCompositionsSave.remove(i);
                    i--;
                }
            }
        }
        workDayDTO.setProductCompositionSaveDTOS(productCompositionSaveDTOS);
        return workDayDTO;
    }
    public WorkDayDTO getByInoutProduct(List<InoutProduct> inoutProducts, List<WarehouseOfProducts> warehouseOfProductsList, WorkDayDTO workDayDTO) {
        List<InoutProductDTO> inoutProductDTOS = new ArrayList<>();
        List<WarehouseOfProducts> warehouseOfProductse = new ArrayList<>(warehouseOfProductsList);
        for (WarehouseOfProducts warehouseOfProducts : warehouseOfProductse) {
            for (int i = 0; i < inoutProducts.size(); i++) {
                if (inoutProducts.get(i).getWarehouseOfProducts().getId().equals(warehouseOfProducts.getId())) {
                    InoutProductDTO inoutProductDTO = inoutProductHasOrNo(inoutProductDTOS, warehouseOfProducts.getId());
                    inoutProductDTO.setProductDTO(wareHouseProductToDTO(warehouseOfProducts));
                    inoutProductDTO.setPrice(
                            calculateBodyPrice(checkNullOther(inoutProducts.get(i).getTotalPrice()),
                                    inoutProducts.get(i).getWeight(),
                                    checkNullOther(inoutProductDTO.getTotalPrice()),
                                    checkNullOther(inoutProductDTO.getWeight()))
                    );
                    inoutProductDTO.setTotalPrice(checkNullOther(inoutProductDTO.getTotalPrice()).add(inoutProducts.get(i).getTotalPrice()));
                    inoutProductDTO.setWeight(checkNullOther(inoutProductDTO.getWeight()).add(inoutProducts.get(i).getWeight()));
                    inoutProductDTO.setCreatedDate(inoutProducts.get(i).getCreatedDate());
                    inoutProductDTO.setId(inoutProducts.get(i).getId());
                    workDayDTO.setInoutAllPrice(checkNullOther(workDayDTO.getInoutAllPrice()).add(inoutProducts.get(i).getTotalPrice()));
                    if (!inoutProductDTOS.contains(inoutProductDTO)) {
                        inoutProductDTOS.add(inoutProductDTO);
                    }
                    inoutProducts.remove(i);
                    i--;
                }
            }
        }
        workDayDTO.setInoutProductDTOS(inoutProductDTOS);
        return workDayDTO;
    }

    public WorkDayDTO getByOutputProduct(List<OutputProduct> outputProtucts, List<WarehouseOfProducts> warehouseOfProductsList, WorkDayDTO workDayDTO) {
        List<OutputProductDTO> outputProductDTOS = new ArrayList<>();
        List<WarehouseOfProducts> warehouseOfProductse = new ArrayList<>(warehouseOfProductsList);
        for (WarehouseOfProducts warehouseOfProducts : warehouseOfProductse) {
            for (int i = 0; i < outputProtucts.size(); i++) {
                if (outputProtucts.get(i).getWarehouseOfProducts().getId().equals(warehouseOfProducts.getId())) {
                    OutputProductDTO outputProductDTO = outputProductHasOrNo(outputProductDTOS, warehouseOfProducts.getId());
                    outputProductDTO.setProductDTO(wareHouseProductToDTO(warehouseOfProducts));
                    outputProductDTO.setPrice(
                            checkNullOther(outputProtucts.get(i).getPrice())
                    );
                    outputProductDTO.setTotalPrice(checkNullOther(outputProductDTO.getTotalPrice()).add(checkNullOther(outputProtucts.get(i).getPrice().multiply(outputProtucts.get(i).getWeight()))));
                    outputProductDTO.setWeight(checkNullOther(outputProductDTO.getWeight()).add(outputProtucts.get(i).getWeight()));
                    outputProductDTO.setCreatedDate(outputProtucts.get(i).getCreatedDate());
                    outputProductDTO.setId(outputProtucts.get(i).getId());
                    workDayDTO.setOutputAllPrice(checkNullOther(workDayDTO.getOutputAllPrice()).add(checkNullOther(outputProtucts.get(i).getPrice())));
                    if (!outputProductDTOS.contains(outputProductDTO)) {
                        outputProductDTOS.add(outputProductDTO);
                    }
                    outputProtucts.remove(i);
                    i--;
                }
            }
        }
        workDayDTO.setOutputProductDTOS(outputProductDTOS);
        return workDayDTO;
    }

    public BigDecimal checkNullOther(BigDecimal any) {
        if (any == null) {
            return BigDecimal.ZERO;
        }
        return any;
    }

    public InoutProductDTO inoutProductHasOrNo(List<InoutProductDTO> inoutProductDTOS, Integer id) {
        for (InoutProductDTO inoutProductDTO : inoutProductDTOS) {
            if (inoutProductDTO.getProductDTO().getId().equals(id)) {
                return inoutProductDTO;
            }
        }
        return new InoutProductDTO();
    }

    public OutputProductDTO outputProductHasOrNo(List<OutputProductDTO> outputProductDTOS, Integer id) {
        for (OutputProductDTO outputProductDTO : outputProductDTOS) {
            if (outputProductDTO.getProductDTO().getId().equals(id)) {
                return outputProductDTO;
            }
        }
        return new OutputProductDTO();
    }
    public ProductCompositionSaveDTO productCompositionHasOrNo(List<ProductCompositionSaveDTO> outputProductDTOS, Integer id) {
        for (ProductCompositionSaveDTO outputProductDTO : outputProductDTOS) {
            if (outputProductDTO.getProductDTO().getId().equals(id)) {
                return outputProductDTO;
            }
        }
        return new ProductCompositionSaveDTO();
    }

    public BigDecimal calculateBodyPrice(BigDecimal price, BigDecimal weight, BigDecimal oldPrice, BigDecimal oldWeight) {
        BigDecimal addWeight = weight.add(oldWeight);
        BigDecimal addedPrices = price.add(oldPrice);
        return addedPrices.divide(addWeight, RoundingMode.HALF_EVEN).setScale(2,RoundingMode.HALF_EVEN);
    }

}
