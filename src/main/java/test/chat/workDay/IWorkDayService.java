package test.chat.workDay;

import java.util.List;

public interface IWorkDayService {
    WorkDayDTO getWorkDay(Long time);
    WorkDayDTO getWorkDay(Long start , Long end,Integer restaurantId);
    List<WorkDayDTO> getWorkDays(Long start , Long end,Integer restaurantId);

}
