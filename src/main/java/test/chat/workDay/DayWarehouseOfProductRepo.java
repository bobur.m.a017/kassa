package test.chat.workDay;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public interface DayWarehouseOfProductRepo extends JpaRepository<DayWareHouseOfProducts,Integer> {
    List<DayWareHouseOfProducts> findAllByWorkDayIn(List<WorkDay> workDays);
    List<DayWareHouseOfProducts> findAllByWorkDay(WorkDay workDays);
}
