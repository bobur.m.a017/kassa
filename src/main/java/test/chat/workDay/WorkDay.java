package test.chat.workDay;

import com.fasterxml.jackson.annotation.JsonIgnore;
import jakarta.persistence.*;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import test.chat.allClasses.SuperClass;
import test.chat.inoutProduct.InoutProduct;
import test.chat.order.Orders;
import test.chat.outputProduct.OutputProduct;
import test.chat.productComposFromSave.ProductCompositionSave;
import test.chat.restaurant.Restaurant;
import test.chat.warehouseOfProducts.WarehouseOfProducts;

import java.util.List;

@Entity
@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
public class WorkDay extends SuperClass {
    private Integer year;
    private Integer month;
    private Integer day;
    @ManyToOne
    @JsonIgnore
    private Restaurant restaurant;

    @OneToMany
    @JoinColumn(name = "work_day_id",referencedColumnName = "id")
    private List<InoutProduct> inoutProduct;

    @OneToMany
    @JoinColumn(name = "work_day_id",referencedColumnName = "id")
    private List<OutputProduct> outputProducts;

    @OneToMany
    @JoinColumn(name = "work_day_id",referencedColumnName = "id")
    private List<Orders> orders;

    @OneToMany
    @JoinColumn(name = "work_day_id",referencedColumnName = "id")
    private List<ProductCompositionSave> productCompositionSaves;

    @OneToMany
    @JoinColumn(name = "work_day_id",referencedColumnName = "id")
    private List<DayWareHouseOfProducts> dayWareHouseOfProducts;

    public WorkDay(Integer year, Integer month, Integer day, Restaurant restaurant, List<InoutProduct> inoutProduct, List<Orders> orders) {
        this.year = year;
        this.month = month;
        this.day = day;
        this.restaurant = restaurant;
        this.inoutProduct = inoutProduct;
        this.orders = orders;
    }
}
