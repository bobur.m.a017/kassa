package test.chat.attachment;

import org.springframework.data.jpa.repository.JpaRepository;

public interface AttachmentRepository extends JpaRepository<Attachment, Integer> {
    Attachment findByFileOriginalName(String fileOriginalName);
}
