package test.chat.attachment;

import jakarta.persistence.Entity;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import test.chat.allClasses.SuperClass;


@Entity
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class Attachment extends SuperClass {


    private String path;  // papkani ichidan topish un

    //CLIENTGA KO'RINADIGAN NOM
    private String fileOriginalName;

    //PAPKADA KO'RINADIGAN NOMI UNIQUE BO'LADI
    private String name;  //izlaganda shu nom bilan izlaniladi
    private Long size;
    private String contentType;
}
