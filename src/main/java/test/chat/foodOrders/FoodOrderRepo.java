package test.chat.foodOrders;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface FoodOrderRepo extends JpaRepository<FoodOrders,Integer> {
//    List<FoodOrders> findAllByFoodIdIn(List<Integer> ids);

    @Modifying
    @Query("DELETE FROM FoodOrders b WHERE b.id = ?1")
    void deleteFoodOrdersByIdqqqq(Integer id);
    void deleteById(Integer id);
}
