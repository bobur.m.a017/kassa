package test.chat.foodOrders;

import jakarta.validation.constraints.NotNull;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import test.chat.food.FoodDTO;

import java.math.BigDecimal;


@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class FoodOrderDTO {
    private  Integer id;
    @NotNull
    private BigDecimal prices;
    private BigDecimal price;
    private Integer count;
    private FoodDTO foodDTO;

    public FoodOrderDTO(Integer id,BigDecimal price,BigDecimal prices,FoodDTO foodDTO,Integer count) {
        this.id = id;
        this.price = price;
        this.prices = prices;
        this.foodDTO = foodDTO;
        this.count = count;
    }

}
