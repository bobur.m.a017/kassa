package test.chat.foodOrders;

import test.chat.food.FoodDTO;
import test.chat.foodFromSave.FoodFromSave;
import test.chat.parsers.IParsersServices;

import java.math.BigDecimal;
import java.math.RoundingMode;

public interface FoodOrderParser extends IParsersServices {
    default FoodOrderDTO foodOrderToDTO(FoodOrders orders,FoodDTO foodDTO){
        return new FoodOrderDTO(
                checkNull(orders.getId()),
                checkNull(orders.getPrice()),
                checkNull(orders.getPrices()),
                checkNull(foodDTO),
                checkNull(orders.getCount())
        );
    }
    default FoodOrders DTOToFoodOrder(FoodOrderDTO foodOrderDTO, FoodFromSave food){
        return new FoodOrders(
                foodOrderDTO.getCount(),
                food,
                food.getPrice(),
                foodOrderDTO.getPrices(),
                food.getBodyPrice().multiply(BigDecimal.valueOf(foodOrderDTO.getCount()))
        );
    }
}
