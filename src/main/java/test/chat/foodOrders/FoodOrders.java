package test.chat.foodOrders;

import com.fasterxml.jackson.annotation.JsonIgnore;
import jakarta.persistence.*;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import test.chat.allClasses.SuperClass;
import test.chat.foodFromSave.FoodFromSave;
import test.chat.order.Orders;

import java.math.BigDecimal;
@Entity
@Getter
@Setter
@NoArgsConstructor
public class FoodOrders extends SuperClass {
    @Column(nullable = false)
    private Integer count;

    @OneToOne(optional = false,cascade = CascadeType.ALL)
    @JsonIgnore
    private FoodFromSave foodFromSave;

    @Column(nullable = false,precision = 19, scale = 6)
    private BigDecimal price;
    @Column(nullable = false,precision = 19, scale = 6)
    private BigDecimal prices;
    @Column(nullable = false,precision = 19, scale = 6)
    private BigDecimal bodyPrice;
    @JsonIgnore
    @ManyToOne
    @JoinColumn(name = "order_id")
    private Orders orders;

    public FoodOrders(Integer count, FoodFromSave foodFromSave, BigDecimal price, BigDecimal prices, BigDecimal bodyPrice) {
        this.count = count;
        this.foodFromSave = foodFromSave;
        this.price = price;
        this.prices = prices;
        this.bodyPrice = bodyPrice;
    }
}
