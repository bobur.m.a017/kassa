package test.chat.statusWork;

import test.chat.food.FoodDTO;

import java.util.List;

public interface IStatusWorkParser {
    default StatusWork dtoToStatusWork(StatusWorkDTO statusWorkDTO){
        return new StatusWork(
                statusWorkDTO.getStatus(),
                statusWorkDTO.getDeadLine()
        );
    }
    default StatusWorkDTO statusToDTO(StatusWork statusWork){
        return new StatusWorkDTO(
                statusWork.getId(),
                statusWork.getStatus(),
                statusWork.getDeadLine()
        );
    }

}
