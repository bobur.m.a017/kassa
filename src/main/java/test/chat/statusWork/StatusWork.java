package test.chat.statusWork;

import com.fasterxml.jackson.annotation.JsonIgnore;
import jakarta.persistence.*;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import test.chat.allClasses.SuperClass;
import test.chat.restaurant.Restaurant;

import java.sql.Timestamp;

@Entity
@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
public class StatusWork extends SuperClass {
    private String status;
    private Timestamp deadLine;

    @OneToOne(mappedBy = "statusWork")
    @JsonIgnore
    private Restaurant restaurant;

    public StatusWork(String status, Timestamp deadLine) {
        this.status = status;
        this.deadLine = deadLine;
    }
}
