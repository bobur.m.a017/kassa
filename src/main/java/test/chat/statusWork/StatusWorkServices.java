package test.chat.statusWork;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import test.chat.food.Food;
import test.chat.food.FoodDTO;
import test.chat.food.IFoodParser;
import test.chat.message.StateMessage;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Service
@RequiredArgsConstructor
public class StatusWorkServices implements IStatusWorkServices,IStatusWorkParser {
    private final StatusWorkRepo statusWorkRepo;

    @Override
    public StateMessage addStatusWork(StatusWorkDTO statusWorkDTO) {
        if (statusWorkDTO.getStatus() != null) {
            statusWorkRepo.save(new StatusWork(statusWorkDTO.getStatus(),statusWorkDTO.getDeadLine()));
        return new StateMessage("Qo'shildi", true, 200);
        }
        return new StateMessage("Qo'shilmadi", true, 500);
    }

    @Override
    public StateMessage editeStatusWork(StatusWorkDTO statusWorkDTO) {
        Optional<StatusWork> statusWorkOptional = statusWorkRepo.findById(statusWorkDTO.getId());
        StatusWork statusWork;
        if (statusWorkOptional.isPresent()) {
            statusWork = statusWorkOptional.get();
            statusWork.setDeadLine(statusWorkDTO.getDeadLine());
            statusWorkRepo.save(statusWork);
            return new StateMessage("O'zgartirildi", true, 200);
        }
        return new StateMessage("O'zgartirildi", true, 200);
    }

    @Override
    public StateMessage deleteStatusWork(Integer id) {
            return new StateMessage("O'chirilmadi", true, 500);
    }

    @Override
    public StatusWorkDTO getStatusWork(Integer restaurantId) {
        StatusWork statusWork = statusWorkRepo.findByRestaurant_Id(restaurantId).orElse(null);
        if (statusWork ==null){
            return null;
        }
        return statusToDTO(statusWork);
    }


}
