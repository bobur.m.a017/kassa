package test.chat.statusWork;

import test.chat.message.StateMessage;


public interface IStatusWorkServices {
    StateMessage addStatusWork(StatusWorkDTO statusWorkDTO);

    StateMessage editeStatusWork(StatusWorkDTO statusWorkDTO);

    StateMessage deleteStatusWork(Integer id);

    StatusWorkDTO getStatusWork(Integer categoryId);
}
