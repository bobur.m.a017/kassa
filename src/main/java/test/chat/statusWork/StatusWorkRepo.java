package test.chat.statusWork;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface StatusWorkRepo extends JpaRepository<StatusWork,Integer> {
    Optional<StatusWork> findByRestaurant_Id(Integer restaurantId);
}
