package test.chat.user;

import test.chat.message.StateMessage;

import java.util.List;

public interface IUserService {
    UserDTO getUser(Integer userId);
    List<UserDTO> getUsers();
    StateMessage addUser(UserSignUpDTO userSignUpDTO);
    StateMessage editeState(UserDTO userSignUpDTO);
    StateMessage addUserToRestaurant(UserSignUpDTO userSignUpDTO,Integer restaurantId);
    StateMessage editeUser(Integer userId,UserSignUpDTO userSignUpDTO);
    StateMessage deleteUser(Integer userId);
}
