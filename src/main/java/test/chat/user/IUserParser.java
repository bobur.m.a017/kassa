package test.chat.user;


import test.chat.role.Role;

import java.util.ArrayList;
import java.util.List;

public interface IUserParser {
    default UserDTO parseUser(Users users) {
        return new UserDTO(
                users.getUsername(),
                users.getName(),
                users.getSurname(),
                users.getFatherName()
        );
    }


    default UserDTO parseUserFromOther(Users users) {
        return new UserDTO(
                users.getUsername(),
                users.getName(),
                users.getSurname(),
                users.getFatherName()
        );
    }

    default Users dtoToUser(UserSignUpDTO userDTO, Role role){
        return new Users(
                userDTO.getName(),
                userDTO.getPassword(),
                userDTO.getUsername(),
                role
        );
    }

}
