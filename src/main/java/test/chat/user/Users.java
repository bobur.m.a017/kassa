package test.chat.user;

import com.fasterxml.jackson.annotation.JsonIgnore;
import jakarta.persistence.*;
import lombok.*;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import test.chat.allClasses.SuperClass;
import test.chat.company.Company;
import test.chat.restaurant.Restaurant;
import test.chat.role.Role;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

@Entity
@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
public class Users extends SuperClass implements UserDetails {

    @Column(unique = true)
    private String username;
    private String password;
    private String name;
    private String fatherName;
    private String surname;
    private Integer age;
    private Boolean isActive = true;
    @ManyToOne
    @JsonIgnore
    private Restaurant restaurant;
    @OneToOne(mappedBy = "user")
    private Company company;



    public Users(String name, String password, String username, Role role) {
        this.name = name;
        this.username = username;
        this.password = password;
        this.role = role;
    }


    public Users(String name, String surname, String fatherName, String password, String username, Role role) {
        this.name = name;
        this.surname = surname;
        this.fatherName = fatherName;
        this.username = username;
        this.password = password;
        this.role = role;

    }

    @ManyToOne
    private Role role;

    @Override
    public Collection<? extends GrantedAuthority> getAuthorities() {
        List<GrantedAuthority> roleList = new ArrayList<>();
        roleList.add(() -> role.getAuthority());
        return roleList;
    }

    @Override
    public String getPassword() {
        return this.password;
    }

    @Override
    public String getUsername() {
        return this.username;
    }

    @Override
    public boolean isAccountNonExpired() {
        return false;
    }

    @Override
    public boolean isAccountNonLocked() {
        return true;
    }

    @Override
    public boolean isCredentialsNonExpired() {
        return true;
    }

    @Override
    public boolean isEnabled() {
        return isActive;
    }
}
