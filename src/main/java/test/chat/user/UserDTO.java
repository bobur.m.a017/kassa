package test.chat.user;

import jakarta.persistence.Column;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class UserDTO {
    private Integer id;
    @Column(nullable = false)
    private String username;

    @Column(nullable = false)
    private String password;

    @Column(nullable = false)
    private String name;
    private String surname;
    private String fatherName;
    private String roleName;
    private Boolean state;

    public UserDTO(String username, String name, String surname, String fatherName) {
        this.username = username;
        this.name = name;
        this.surname = surname;
        this.fatherName = fatherName;
    }
    public UserDTO(String username, String name, String surname, String fatherName,Boolean state) {
        this.username = username;
        this.name = name;
        this.surname = surname;
        this.fatherName = fatherName;
        this.state = state;
    }
}
