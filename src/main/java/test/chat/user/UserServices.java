package test.chat.user;

import lombok.RequiredArgsConstructor;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;
import test.chat.company.Company;
import test.chat.company.CompanyRepo;
import test.chat.conf.PasswordEncoders;
import test.chat.message.StateMessage;
import test.chat.restaurant.Restaurant;
import test.chat.restaurant.RestaurantRepo;
import test.chat.role.Role;
import test.chat.role.RoleRepo;
import test.chat.role.RoleType;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Service
@RequiredArgsConstructor
public class UserServices implements UserDetailsService, IUserService, IUserParser {
    private final UserRepo userRepo;
    private final RoleRepo roleRepo;
    private final CompanyRepo companyRepo;
    private final RestaurantRepo restaurantRepo;
    private final PasswordEncoders passwordEncoder;

    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        Users users = userRepo.findByUsername(username).orElseThrow(() -> new UsernameNotFoundException("User not found"));
        return new User(users.getUsername(), users.getPassword(), users.getAuthorities());
    }

    @Override
    public UserDTO getUser(Integer userId) {
        Optional<Users> byId = userRepo.findById(userId);
        return byId.map(this::parseUser).orElse(null);
    }

    @Override
    public List<UserDTO> getUsers() {
        List<Users> allByRoleName = userRepo.findAllByRole_Name(RoleType.ROLE_USER.getName());
        List<UserDTO> userDTOS = new ArrayList<>();
        for (Users users : allByRoleName) {
            userDTOS.add(parseUser(users));
        }
        return userDTOS;
    }

    @Override
    public StateMessage addUser(UserSignUpDTO userSignUpDTO) {
        Company company = new Company("Kompaniya " + userSignUpDTO.getSurname(), null,
                new Users(
                        userSignUpDTO.getName(),
                        userSignUpDTO.getSurname(),
                        userSignUpDTO.getFatherName(),
                        passwordEncoder.passwordEncoder().encode(userSignUpDTO.getPassword()),
                        userSignUpDTO.getUsername(),
                        roleRepo.findByName(RoleType.ROLE_DIRECTOR.getName()).orElse(null)

                ));
        companyRepo.save(company);
        return new StateMessage("Amalga oshirildi", true, 200);
    }

    @Override
    public StateMessage editeState(UserDTO userSignUpDTO) {
        Users users = userRepo.findById(userSignUpDTO.getId()).orElse(null);
        if (users == null){
            return new StateMessage("User topilmadi", true, 404);
        }
        users.setIsActive(!userSignUpDTO.getState());
        userRepo.save(users);
        return new StateMessage("Amalga oshirildi", true, 200);
    }


    @Override
    public StateMessage addUserToRestaurant(UserSignUpDTO userSignUpDTO, Integer restaurantId) {
        Role role = roleRepo.findByName(RoleType.ROLE_DIRECTOR.getName()).orElse(null);
        Users users = dtoToUser(userSignUpDTO, role);
        Restaurant restaurant = restaurantRepo.findById(restaurantId).orElse(null);
        if (restaurant == null) {
            return new StateMessage("Role topilmadi", true, 404);
        }
        restaurant.setRestaurantsUser(List.of(users));
        restaurantRepo.save(restaurant);
        return new StateMessage("Amalga oshirildi", true, 200);
    }

    @Override
    public StateMessage editeUser(Integer userId,UserSignUpDTO userSignUpDTO) {
        Optional<Users> byUsername = userRepo.findByUsername(userSignUpDTO.getUsername());
        if (byUsername.isEmpty()) {
            return new StateMessage("пользователь ненайдено", false, 404);
        } else {
            Users users = byUsername.get();
            users.setName(userSignUpDTO.getName());
            users.setFatherName(userSignUpDTO.getFatherName());
            users.setSurname(userSignUpDTO.getSurname());
            users.setSurname(userSignUpDTO.getSurname());
            return new StateMessage("пользователь изменено", true, 200);
        }
    }

    @Override
    public StateMessage deleteUser(Integer userId) {
        userRepo.deleteById(userId);
        return new StateMessage("пользователь удалено", true, 200);
    }
}
