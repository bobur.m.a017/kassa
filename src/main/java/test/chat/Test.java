package test.chat;

import test.chat.statics.StaticMethods;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.sql.Timestamp;
import java.time.LocalDateTime;

public class Test {
    public static void main(String[] args) {
        BigDecimal bigDecimal2 = new BigDecimal(30);
        BigDecimal bigDecimal3 = new BigDecimal(30);
        System.out.println(bigDecimal3.compareTo(bigDecimal2));
        bigDecimal3 = bigDecimal3.divide(bigDecimal2,1,RoundingMode.HALF_EVEN);
        System.out.println(bigDecimal3);
        Timestamp startCurrentDate = StaticMethods.getStartCurrentDate();
        LocalDateTime localDateTime = startCurrentDate.toLocalDateTime();
        System.out.println(localDateTime.getDayOfMonth());
        System.out.println(localDateTime.getMonth().getValue());
        System.out.println(localDateTime.getYear());
    }
}
