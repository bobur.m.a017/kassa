package test.chat.statics;

import com.itextpdf.text.Element;
import com.itextpdf.text.Font;
import com.itextpdf.text.Phrase;
import com.itextpdf.text.Rectangle;
import com.itextpdf.text.pdf.PdfPCell;
import org.apache.poi.ss.usermodel.CellStyle;
import org.apache.poi.xssf.usermodel.XSSFCellStyle;
import org.apache.poi.xssf.usermodel.XSSFFont;

import java.sql.Timestamp;
import java.time.ZonedDateTime;
import java.time.temporal.ChronoUnit;
import java.util.Calendar;
import java.util.List;

public class StaticMethods {
    public static Long getMillisecondCurrentDay(){
        ZonedDateTime zonedDateTime = ZonedDateTime.now();
        zonedDateTime = zonedDateTime.truncatedTo(ChronoUnit.DAYS);
        return zonedDateTime.toInstant().toEpochMilli();
    }
    public static Timestamp getStartCurrentDate(){
        return new Timestamp(getMillisecondCurrentDay());
    }
    public static Timestamp addDayToMillisecond(Integer day){
        Calendar calendar = Calendar.getInstance();
        calendar.setTimeInMillis(StaticMethods.getMillisecondCurrentDay());
        calendar.add(Calendar.DAY_OF_MONTH, day);
        return new Timestamp(calendar.getTimeInMillis());
    }
    public static PdfPCell createCell(String content, int rowspan, int size, int colspan) {


        PdfPCell cell = new PdfPCell(new Phrase(content, new Font(Font.FontFamily.TIMES_ROMAN, size)));
        cell.setColspan(colspan);
        cell.setRowspan(rowspan);
        cell.setBorder(Rectangle.BOX);
        cell.setHorizontalAlignment(Element.ALIGN_CENTER);
        cell.setVerticalAlignment(Element.ALIGN_MIDDLE);
        return cell;
    }

    public static void style(XSSFFont font, XSSFCellStyle cellStyleCouple) {
        cellStyleCouple.setFont(font);
        cellStyleCouple.setAlignment(CellStyle.ALIGN_CENTER);
        cellStyleCouple.setVerticalAlignment(CellStyle.VERTICAL_CENTER);
        cellStyleCouple.setBorderBottom(CellStyle.BORDER_THIN);
        cellStyleCouple.setBorderLeft(CellStyle.BORDER_THIN);
        cellStyleCouple.setBorderRight(CellStyle.BORDER_THIN);
        cellStyleCouple.setBorderTop(CellStyle.BORDER_THIN);
    }
    public static  <T> List<T> checkListEmpty(List<T> list){
        if (list.isEmpty()){
            return null;
        }else {
            return list;
        }
    }
    public static  <T> T checkNull(T obj) {
        if (obj != null) return obj;
        return null;
    }
}
