package test.chat.dataLoader;

import jakarta.annotation.PostConstruct;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Component;
import test.chat.company.Company;
import test.chat.company.CompanyRepo;
import test.chat.restaurant.Restaurant;
import test.chat.restaurant.StatusTypes;
import test.chat.role.Role;
import test.chat.role.RoleRepo;
import test.chat.role.RoleType;
import test.chat.statusWork.StatusWork;
import test.chat.user.UserRepo;
import test.chat.user.Users;

import java.sql.Timestamp;
import java.time.Instant;
import java.time.ZonedDateTime;
import java.time.temporal.ChronoUnit;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.TimeZone;
import java.util.concurrent.TimeUnit;

@Component
@RequiredArgsConstructor
public class DataLoader implements CommandLineRunner {
    private final UserRepo userRepo;
    private final RoleRepo roleRepo;
    private final CompanyRepo companyRepo;
    private final PasswordEncoder passwordEncoder;

    @Override
    public void run(String... args) throws Exception {
        saveRole();
        saveUser();
        init();

    }

    @PostConstruct
    public void init() {
        TimeZone.setDefault(TimeZone.getTimeZone("Asia/Tashkent"));
    }

    public void saveUser() {
        System.out.println("user    "+passwordEncoder.encode("1234governessKasser"));
        System.out.println("admin    "+passwordEncoder.encode("1234adminKassa"));
        System.out.println("super admin    "+passwordEncoder.encode("1234superAdminKassa"));
        Optional<Users> adminKassa = userRepo.findByUsername("adminKassa");
        if (userRepo.findByUsername("adminKassa").isEmpty()) {
            Restaurant restaurant = new Restaurant("Nurafshon",new StatusWork(StatusTypes.AUTO_MANUAL.getName(), null));
            Users kasser =new Users("Kassa", passwordEncoder.encode("1234governessKasser"), "governessKasser", roleRepo.findByName(RoleType.ROLE_USER.getName()).get());
            Users dostavkaKasser = new Users("Dostavka kassa", passwordEncoder.encode("1234governessKasserSet"), "governessKasserSet", roleRepo.findByName(RoleType.ROLE_DELIVERY_ADMIN.getName()).get());
            userRepo.save(new Users("Super Admin", passwordEncoder.encode("1234superAdminKassa"), "superAdminKassa", roleRepo.findByName(RoleType.SUPER_ADMIN.getName()).get()));
            Users restaurantAdmin = new Users("admin", passwordEncoder.encode("1234adminKassa"), "adminKassa", roleRepo.findByName(RoleType.ROLE_ADMIN.getName()).get());
            List<Users> users = new ArrayList<>();
            users.add(kasser);
            users.add(dostavkaKasser);
            users.add(restaurantAdmin);
            restaurant.setRestaurantsUser(users);
            Company company = new Company(
                    "Governess",List.of(restaurant),new Users("Direktor", passwordEncoder.encode("1234adminKassaDirektor"), "adminKassaDirektor", roleRepo.findByName(RoleType.ROLE_DIRECTOR.getName()).get()));
            companyRepo.save(company);
        }
    }

    public void saveRole() {
        if (roleRepo.findByName(RoleType.ROLE_USER.getName()).isEmpty()) {
            roleRepo.save(new Role(RoleType.ROLE_USER.getName(),RoleType.ROLE_USER.getRoleName()));
            roleRepo.save(new Role(RoleType.SUPER_ADMIN.getName(),RoleType.SUPER_ADMIN.getRoleName()));
            roleRepo.save(new Role(RoleType.ROLE_ADMIN.getName(),RoleType.ROLE_ADMIN.getRoleName()));
            roleRepo.save(new Role(RoleType.ROLE_DIRECTOR.getName(),RoleType.ROLE_DIRECTOR.getRoleName()));
            roleRepo.save(new Role(RoleType.ROLE_DELIVERY_ADMIN.getName(),RoleType.ROLE_DELIVERY_ADMIN.getRoleName()));
        }
    }
}
