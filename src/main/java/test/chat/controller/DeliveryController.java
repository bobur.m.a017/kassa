package test.chat.controller;

import jakarta.servlet.http.HttpServletRequest;
import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;
import test.chat.conf.JwtServices;
import test.chat.delivery.DeliveryDTO;
import test.chat.delivery.IDeliveryService;
import test.chat.message.StateMessage;


@RequiredArgsConstructor
@RequestMapping("/api/v1/delivery")
@RestController
public class DeliveryController {

    private final IDeliveryService categoryServices;
    private final JwtServices jwtServices;

    @PreAuthorize("hasAnyRole('ADMIN','SUPER_ADMIN')")
    @PostMapping
    public ResponseEntity<?> addDelivery(@RequestBody DeliveryDTO deli, HttpServletRequest request) {
        String token = jwtServices.getToken(request);
        StateMessage stateMessage = categoryServices.addDelivery(deli, jwtServices.getOfficeId(token));
        return ResponseEntity.status(stateMessage.getCode()).body(stateMessage.getText());
    }
    @PreAuthorize("hasAnyRole('ADMIN','SUPER_ADMIN')")
    @PutMapping
    public ResponseEntity<?> editeDelivery(@RequestBody DeliveryDTO deli) {
        StateMessage stateMessage = categoryServices.editeDelivery(deli);
        return ResponseEntity.status(stateMessage.getCode()).body(stateMessage.getText());
    }

    @PreAuthorize("hasAnyRole('ADMIN','SUPER_ADMIN')")
    @DeleteMapping("/{id}")
    public ResponseEntity<?> deleteDelivery(@PathVariable Integer id) {
        StateMessage stateMessage = categoryServices.deleteDelivery(id);
        return ResponseEntity.status(stateMessage.getCode()).body(stateMessage.getText());
    }

    @PreAuthorize("hasAnyRole('ADMIN','SUPER_ADMIN','USER','DELIVERY_ADMIN','DIRECTOR')")
    @GetMapping
    public ResponseEntity<?> getDeliveries(HttpServletRequest request) {
        String token = jwtServices.getToken(request);
        return ResponseEntity.ok(categoryServices.getDeliveries(jwtServices.getOfficeId(token)));
    }
    @PreAuthorize("hasAnyRole('DELIVERY_ADMIN')")
    @PutMapping("/relations/{id}")
    public ResponseEntity<?> relationsDeliveries(@RequestParam Integer deliveryId,@PathVariable Integer id,HttpServletRequest request) {
        String token = jwtServices.getToken(request);
        return ResponseEntity.ok(categoryServices.relationsDelivery(id,deliveryId));
    }

}
