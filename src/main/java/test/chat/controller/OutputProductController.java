package test.chat.controller;

import jakarta.servlet.http.HttpServletRequest;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;
import test.chat.conf.JwtServices;
import test.chat.inoutProduct.IInoutProductServices;
import test.chat.inoutProduct.InoutProductDTO;
import test.chat.message.StateMessage;
import test.chat.order.IOrderServices;
import test.chat.outputProduct.IOutputProductService;
import test.chat.outputProduct.OutputProductDTO;
import test.chat.warehouseOfProducts.WarehouseOfProductDTO;

import java.util.List;


@RequiredArgsConstructor
@RequestMapping("/api/v1/output-product")
@RestController
public class OutputProductController {
    private final IOrderServices orderServices;
    private final IOutputProductService outputProductService;
    private final JwtServices jwtServices;

    @PreAuthorize("hasAnyRole('ADMIN','SUPER_ADMIN')")
    @PostMapping
    public ResponseEntity<?> addOutProduct(@RequestBody WarehouseOfProductDTO productDTO, HttpServletRequest request) {
        String token = jwtServices.getToken(request);
        StateMessage stateMessage = orderServices.outputProduct(productDTO, jwtServices.getOfficeId(token),jwtServices.getUsername(token));
        return ResponseEntity.status(stateMessage.getCode()).body(stateMessage.getText());
    }

    @PreAuthorize("hasAnyRole('ADMIN','SUPER_ADMIN')")
    @PutMapping("/{id}")
    public ResponseEntity<?> editeOutProduct(@PathVariable Integer id,@RequestBody WarehouseOfProductDTO productDTO, HttpServletRequest request) {
        String token = jwtServices.getToken(request);
        StateMessage stateMessage = orderServices.editeOutputProduct(id,productDTO, jwtServices.getOfficeId(token),jwtServices.getUsername(token));
        return ResponseEntity.status(stateMessage.getCode()).body(stateMessage.getText());
    }
    @PreAuthorize("hasAnyRole('ADMIN','SUPER_ADMIN')")
    @GetMapping
    public ResponseEntity<?> getOutProducts(@RequestParam(required = false) Long startDate,@RequestParam(required = false) Long endDate, HttpServletRequest request) {
        String token = jwtServices.getToken(request);
        List<OutputProductDTO> outputProducts = outputProductService.getOutputProducts(jwtServices.getOfficeId(token), startDate, endDate);
        return ResponseEntity.ok(outputProducts);
    }
}
