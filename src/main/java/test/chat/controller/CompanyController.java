package test.chat.controller;

import jakarta.servlet.http.HttpServletRequest;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.web.bind.annotation.*;
import test.chat.company.CompanyDTO;
import test.chat.company.ICompanyServices;
import test.chat.conf.JwtServices;
import test.chat.message.StateMessage;


@RequiredArgsConstructor
@RequestMapping("/api/v1/company")
@RestController
public class CompanyController {

    private final ICompanyServices companyServices;
    private final JwtServices jwtServices;

    @PreAuthorize("hasAnyRole('ADMIN','SUPER_ADMIN')")
    @PostMapping
    public ResponseEntity<?> addCompany(@RequestBody CompanyDTO companyDTO, HttpServletRequest request) {
        String token = jwtServices.getToken(request);
        StateMessage stateMessage = companyServices.addCompany(companyDTO, jwtServices.getUsername(token));
        return ResponseEntity.status(stateMessage.getCode()).body(stateMessage.getText());
    }

    @PreAuthorize("hasAnyRole('ADMIN','SUPER_ADMIN')")
    @PutMapping
    public ResponseEntity<?> editeCompany(@RequestBody CompanyDTO companyDTO) {
        StateMessage stateMessage = companyServices.editeCompany(companyDTO);
        return ResponseEntity.status(stateMessage.getCode()).body(stateMessage.getText());
    }

    @PreAuthorize("hasAnyRole('ADMIN','SUPER_ADMIN')")
    @DeleteMapping("/{id}")
    public ResponseEntity<?> deleteCompany(@PathVariable Integer id) {
        StateMessage stateMessage = companyServices.deleteCompany(id);
        return ResponseEntity.status(stateMessage.getCode()).body(stateMessage.getText());
    }

    @PreAuthorize("hasAnyRole('ADMIN','SUPER_ADMIN','USER','DELIVERY_ADMIN','DIRECTOR')")
    @GetMapping
    public ResponseEntity<?> getCompanys() {
        return ResponseEntity.ok(companyServices.getCompanies());
    }

    @PreAuthorize("hasAnyRole('ADMIN','SUPER_ADMIN','USER','DELIVERY_ADMIN','DIRECTOR')")
    @GetMapping("/{id}")
    public ResponseEntity<?> getCompany(@PathVariable Integer id) {
        CompanyDTO user = companyServices.getCompany(id);
        if (user != null) {
            return ResponseEntity.ok(user);
        } else {
            return ResponseEntity.status(HttpStatus.NO_CONTENT).body("Not found");
        }
    }

    @PreAuthorize("hasAnyRole('DIRECTOR')")
    @GetMapping("/by")
    public ResponseEntity<?> getCompanyBy(HttpServletRequest request) {
        String token = jwtServices.getToken(request);
        CompanyDTO user = companyServices.getCompany(jwtServices.getOfficeId(token));
        if (user != null) {
            return ResponseEntity.ok(user);
        } else {
            return ResponseEntity.status(HttpStatus.NO_CONTENT).body("Not found");
        }
    }

}
