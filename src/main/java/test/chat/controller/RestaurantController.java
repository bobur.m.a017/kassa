package test.chat.controller;

import jakarta.servlet.http.HttpServletRequest;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;
import test.chat.conf.JwtServices;
import test.chat.message.StateMessage;
import test.chat.restaurant.IRestaurantServices;
import test.chat.restaurant.RestaurantDTO;


@RequiredArgsConstructor
@RequestMapping("/api/v1/restaurant")
@RestController
public class RestaurantController {

    private final IRestaurantServices restaurantServices;
    private final JwtServices jwtServices;

    @PreAuthorize("hasAnyRole('ADMIN','SUPER_ADMIN')")
    @PostMapping
    public ResponseEntity<?> addRestaurant(@RequestBody RestaurantDTO restaurantDTO, HttpServletRequest request) {
        String token = jwtServices.getToken(request);
        StateMessage stateMessage = restaurantServices.addRestaurant(restaurantDTO, jwtServices.getOfficeId(token));
        return ResponseEntity.status(stateMessage.getCode()).body(stateMessage.getText());
    }

    @PreAuthorize("hasAnyRole('ADMIN','SUPER_ADMIN')")
    @PutMapping
    public ResponseEntity<?> editeRestaurant(@RequestBody RestaurantDTO restaurantDTO) {
        StateMessage stateMessage = restaurantServices.editeRestaurant(restaurantDTO);
        return ResponseEntity.status(stateMessage.getCode()).body(stateMessage.getText());
    }

    @PreAuthorize("hasAnyRole('ADMIN','SUPER_ADMIN')")
    @DeleteMapping("/{id}")
    public ResponseEntity<?> deleteRestaurant(@PathVariable Integer id) {
        StateMessage stateMessage = restaurantServices.deleteRestaurant(id);
        return ResponseEntity.status(stateMessage.getCode()).body(stateMessage.getText());
    }

    @PreAuthorize("hasAnyRole('DIRECTOR')")
    @GetMapping("/director")
    public ResponseEntity<?> getRestaurantsDirector(HttpServletRequest request) {
        String token = jwtServices.getToken(request);
        return ResponseEntity.ok(restaurantServices.getRestaurantsForDirector(jwtServices.getOfficeId(token), jwtServices.getUsername(token)));
    }

    @PreAuthorize("hasAnyRole('ADMIN')")
    @GetMapping("/{id}")
    public ResponseEntity<?> getRestaurant(@PathVariable Integer id) {
        RestaurantDTO user = restaurantServices.getRestaurant(id);
        if (user != null) {
            return ResponseEntity.ok(user);
        } else {
            return ResponseEntity.status(HttpStatus.NO_CONTENT).body("Not found");
        }
    }

    @PreAuthorize("hasAnyRole('ADMIN')")
    @GetMapping("/admin")
    public ResponseEntity<?> getRestaurantByAdmin(HttpServletRequest request) {
        String token = jwtServices.getToken(request);
        RestaurantDTO user = restaurantServices.getRestaurant(jwtServices.getOfficeId(token));
        if (user != null) {
            return ResponseEntity.ok(user);
        } else {
            return ResponseEntity.status(HttpStatus.NO_CONTENT).body("Not found");
        }
    }

}
