package test.chat.controller;

import jakarta.servlet.http.HttpServletRequest;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.multipart.MultipartHttpServletRequest;
import test.chat.conf.JwtServices;
import test.chat.food.FoodDTO;
import test.chat.food.IFoodServices;
import test.chat.message.StateMessage;
import test.chat.productComposition.ProductCompositionDTO;

import java.util.List;


@RequiredArgsConstructor
@RequestMapping("/api/v1/food")
@RestController
public class FoodController {

    private final IFoodServices ifoodServices;
    private final JwtServices jwtServices;

    @PreAuthorize("hasAnyRole('ADMIN','SUPER_ADMIN')")
    @PostMapping
    public ResponseEntity<?> addFood(@RequestBody FoodDTO foodDto,HttpServletRequest request) {
        StateMessage stateMessage = ifoodServices.addFood(foodDto, jwtServices.getOfficeId(jwtServices.getToken(request)));
        return ResponseEntity.status(stateMessage.getCode()).body(stateMessage.getText());
    }
    @PreAuthorize("hasAnyRole('ADMIN','SUPER_ADMIN')")
    @PostMapping("/compositions/{id}")
    public ResponseEntity<?> addProductToFood(@PathVariable Integer id, @RequestBody ProductCompositionDTO productCompositionDTO, HttpServletRequest request) {
        StateMessage stateMessage = ifoodServices.addProductCompositionToFood(id, productCompositionDTO,jwtServices.getOfficeId(jwtServices.getToken(request)));
        return ResponseEntity.status(stateMessage.getCode()).body(stateMessage.getText());
    }
    @PreAuthorize("hasAnyRole('ADMIN','SUPER_ADMIN')")
    @PutMapping("/compositions/{id}")
    public ResponseEntity<?> editeProductToFood(@PathVariable Integer id, @RequestBody ProductCompositionDTO productCompositionDTO, HttpServletRequest request) {
        StateMessage stateMessage = ifoodServices.editeProductCompositionToFood(id, productCompositionDTO);
        return ResponseEntity.status(stateMessage.getCode()).body(stateMessage.getText());
    }

    @PreAuthorize("hasAnyRole('ADMIN','SUPER_ADMIN')")
    @PutMapping()
    public ResponseEntity<?> editeFood(@RequestBody FoodDTO foodDto, HttpServletRequest request) {
        StateMessage stateMessage = ifoodServices.editeFood(foodDto, jwtServices.getOfficeId(jwtServices.getToken(request)));
        return ResponseEntity.status(stateMessage.getCode()).body(stateMessage.getText());
    }

    @PreAuthorize("hasAnyRole('ADMIN','SUPER_ADMIN')")
    @DeleteMapping("/{id}")
    public ResponseEntity<?> deleteFood(@PathVariable Integer id) {
        StateMessage stateMessage = ifoodServices.deleteFood(id);
        return ResponseEntity.status(stateMessage.getCode()).body(stateMessage.getText());
    }
    @PreAuthorize("hasAnyRole('ADMIN','SUPER_ADMIN')")
    @DeleteMapping("/compositions/{id}")
    public ResponseEntity<?> deleteProductCompositions(@PathVariable Integer id) {
        StateMessage stateMessage = ifoodServices.deleteProductComposition(id);
        return ResponseEntity.status(stateMessage.getCode()).body(stateMessage.getText());
    }

    @PreAuthorize("hasAnyRole('SUPER_ADMIN','USER','ADMIN','DELIVERY_ADMIN','DIRECTOR')")
    @GetMapping("/by-category")
    @ResponseBody
    public ResponseEntity<?> getFoods(@RequestParam(required = false) Integer categoryId) {
        return ResponseEntity.ok(ifoodServices.getFoods(categoryId));
    }

    @PreAuthorize("hasAnyRole('ADMIN','SUPER_ADMIN')")
    @PutMapping("/relation")
    @ResponseBody
    public ResponseEntity<?> relationFood(@RequestParam Integer categoryId, Integer foodId) {
        StateMessage stateMessage = ifoodServices.relationsToCategory(categoryId, foodId);
        return ResponseEntity.status(stateMessage.getCode()).body(stateMessage.getText());
    }

    @PreAuthorize("hasAnyRole('SUPER_ADMIN','USER','ADMIN','DELIVERY_ADMIN','DIRECTOR')")
    @GetMapping("/{id}")
    public ResponseEntity<?> getFood(@PathVariable Integer id) {
        FoodDTO food = ifoodServices.getFood(id);
        if (food != null) {
            return ResponseEntity.ok(food);
        } else {
            return ResponseEntity.status(HttpStatus.NO_CONTENT).body("Not found");
        }
    }

    @PreAuthorize("hasAnyRole('ADMIN','SUPER_ADMIN')")
    @PostMapping("/add-image/{id}")
    public ResponseEntity<?> addAttachment(@PathVariable Integer id, MultipartHttpServletRequest request) {
        try {
            MultipartFile files = request.getFile("file");
            StateMessage stateMessage = ifoodServices.addAttachment(id, files);
            return ResponseEntity.status(stateMessage.getCode()).body(stateMessage.getText());
        } catch (Exception e) {
            e.printStackTrace();
            return ResponseEntity.status(HttpStatus.EXPECTATION_FAILED).body(e.getMessage());
        }
    }

}
