package test.chat.controller;

import jakarta.servlet.http.HttpServletRequest;
import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;
import test.chat.conf.JwtServices;
import test.chat.delivery.DeliveryDTO;
import test.chat.delivery.IDeliveryService;
import test.chat.message.StateMessage;
import test.chat.workDay.IWorkDayService;


@RequiredArgsConstructor
@RequestMapping("/api/v1/work-day")
@RestController
public class WorkDayController {

    private final IWorkDayService categoryServices;
    private final JwtServices jwtServices;

//    @PreAuthorize("hasAnyRole('ADMIN','SUPER_ADMIN')")
//    @PostMapping
//    public ResponseEntity<?> addDelivery(@RequestBody DeliveryDTO deli, HttpServletRequest request) {
//        String token = jwtServices.getToken(request);
//        StateMessage stateMessage = categoryServices.addDelivery(deli, jwtServices.getOfficeId(token));
//        return ResponseEntity.status(stateMessage.getCode()).body(stateMessage.getText());
//    }
//    @PreAuthorize("hasAnyRole('ADMIN','SUPER_ADMIN')")
//    @PutMapping
//    public ResponseEntity<?> editeDelivery(@RequestBody DeliveryDTO deli) {
//        StateMessage stateMessage = categoryServices.editeDelivery(deli);
//        return ResponseEntity.status(stateMessage.getCode()).body(stateMessage.getText());
//    }

//    @PreAuthorize("hasAnyRole('ADMIN','SUPER_ADMIN')")
//    @DeleteMapping("/{id}")
//    public ResponseEntity<?> deleteDelivery(@PathVariable Integer id) {
//        StateMessage stateMessage = categoryServices.deleteDelivery(id);
//        return ResponseEntity.status(stateMessage.getCode()).body(stateMessage.getText());
//    }

    @PreAuthorize("hasAnyRole('ADMIN','SUPER_ADMIN','USER','DELIVERY_ADMIN','DIRECTOR')")
    @GetMapping
    public ResponseEntity<?> getWorkDay(@RequestParam Long startDate,@RequestParam Long endDate,HttpServletRequest request) {
        String token = jwtServices.getToken(request);
        return ResponseEntity.ok(categoryServices.getWorkDay(startDate,endDate,jwtServices.getOfficeId(token)));
    }
    @PreAuthorize("hasAnyRole('ADMIN','SUPER_ADMIN','USER','DELIVERY_ADMIN','DIRECTOR')")
    @GetMapping("/by-date")
    public ResponseEntity<?> getWorkDays(@RequestParam Long startDate,@RequestParam Long endDate,HttpServletRequest request) {
        String token = jwtServices.getToken(request);
        return ResponseEntity.ok(categoryServices.getWorkDays(startDate,endDate,jwtServices.getOfficeId(token)));
    }


}
