package test.chat.controller;

import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.web.bind.annotation.*;
import test.chat.category.CategoryDTO;
import test.chat.category.ICategoryServices;
import test.chat.message.StateMessage;


@RequiredArgsConstructor
@RequestMapping("/api/v1/category")
@RestController
public class CategoryController {

    private final ICategoryServices categoryServices;
    private AuthenticationManager authenticationManager;

    @PreAuthorize("hasAnyRole('ADMIN','SUPER_ADMIN')")
    @PostMapping
    public ResponseEntity<?> addCategory(@RequestBody CategoryDTO userSignUpDTO) {
        StateMessage stateMessage = categoryServices.addCategory(userSignUpDTO);
        return ResponseEntity.status(stateMessage.getCode()).body(stateMessage.getText());
    }
    @PreAuthorize("hasAnyRole('ADMIN','SUPER_ADMIN')")
    @PutMapping
    public ResponseEntity<?> editeCategory(@RequestBody CategoryDTO userSignUpDTO) {
        StateMessage stateMessage = categoryServices.editeCategory(userSignUpDTO);
        return ResponseEntity.status(stateMessage.getCode()).body(stateMessage.getText());
    }

    @PreAuthorize("hasAnyRole('ADMIN','SUPER_ADMIN')")
    @DeleteMapping("/{id}")
    public ResponseEntity<?> deleteCategory(@PathVariable Integer id) {
        StateMessage stateMessage = categoryServices.deleteCategory(id);
        return ResponseEntity.status(stateMessage.getCode()).body(stateMessage.getText());
    }

    @PreAuthorize("hasAnyRole('ADMIN','SUPER_ADMIN','USER','DELIVERY_ADMIN','DIRECTOR')")
    @GetMapping
    public ResponseEntity<?> getCategorys() {
        return ResponseEntity.ok(categoryServices.getCategories());
    }
    @PreAuthorize("hasAnyRole('ADMIN','SUPER_ADMIN','USER','DELIVERY_ADMIN','DIRECTOR')")
    @GetMapping("/{id}")
    public ResponseEntity<?> getCategory(@PathVariable Integer id) {
        CategoryDTO user = categoryServices.getCategory(id);
        if (user != null) {
            return ResponseEntity.ok(user);
        } else {
            return ResponseEntity.status(HttpStatus.NO_CONTENT).body("Not found");
        }
    }

}
