package test.chat.controller;

import jakarta.servlet.http.HttpServletRequest;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;
import test.chat.conf.JwtServices;
import test.chat.inoutProduct.IInoutProductServices;
import test.chat.inoutProduct.InoutProductDTO;
import test.chat.message.StateMessage;


@RequiredArgsConstructor
@RequestMapping("/api/v1/inout-product")
@RestController
public class InoutProductController {
    private final IInoutProductServices inoutInoutProductService;
    private final JwtServices jwtServices;

    @PreAuthorize("hasAnyRole('ADMIN','SUPER_ADMIN')")
    @PostMapping("/{id}")
    public ResponseEntity<?> addInoutProduct(@PathVariable Integer id, @RequestBody InoutProductDTO productDTO, HttpServletRequest request) {
        String token = jwtServices.getToken(request);
        StateMessage stateMessage = inoutInoutProductService.addInoutProduct(productDTO, id, jwtServices.getUsername(token), jwtServices.getOfficeId(token));
        return ResponseEntity.status(stateMessage.getCode()).body(stateMessage.getText());
    }

    @PreAuthorize("hasAnyRole('ADMIN','SUPER_ADMIN')")
    @PutMapping
    public ResponseEntity<?> editeInoutProduct(@RequestBody InoutProductDTO productDTO, HttpServletRequest request) {
//        String token = jwtServices.getToken(request);
        StateMessage stateMessage = inoutInoutProductService.editeInoutProduct(productDTO);
        return ResponseEntity.status(stateMessage.getCode()).body(stateMessage.getText());
    }

    @PreAuthorize("hasAnyRole('ADMIN','SUPER_ADMIN')")
    @DeleteMapping("/{id}")
    public ResponseEntity<?> deleteInoutProduct(@PathVariable Integer id) {
        StateMessage stateMessage = inoutInoutProductService.deleteInoutProduct(id);
        return ResponseEntity.status(stateMessage.getCode()).body(stateMessage.getText());
    }

    @PreAuthorize("hasAnyRole('SUPER_ADMIN','USER','ADMIN','DELIVERY_ADMIN','DIRECTOR')")
    @GetMapping
    public ResponseEntity<?> getInoutProducts(@RequestParam(required = false) Long startDate, @RequestParam(required = false) Long endDate, HttpServletRequest request) {
        String token = jwtServices.getToken(request);
        return ResponseEntity.ok(inoutInoutProductService.getInoutProducts(startDate, endDate, jwtServices.getOfficeId(token)));
    }

    @PreAuthorize("hasAnyRole('SUPER_ADMIN','USER','ADMIN','DELIVERY_ADMIN','DIRECTOR')")
    @GetMapping("/{id}")
    public ResponseEntity<?> getInoutProduct(@PathVariable Integer id) {
        InoutProductDTO user = inoutInoutProductService.getInoutProduct(id);
        if (user != null) {
            return ResponseEntity.ok(user);
        } else {
            return ResponseEntity.status(HttpStatus.NO_CONTENT).body("Not found");
        }
    }

    @PreAuthorize("hasAnyRole('SUPER_ADMIN','USER','ADMIN','DELIVERY_ADMIN','DIRECTOR')")
    @GetMapping("/everyDay")
    public ResponseEntity<?> everyDay() {
//        inoutInoutProductService.everyday();
        return ResponseEntity.status(HttpStatus.NO_CONTENT).body("Not found");
    }

}
