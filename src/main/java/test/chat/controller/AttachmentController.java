package test.chat.controller;

import jakarta.servlet.http.HttpServletResponse;
import jakarta.validation.constraints.NotNull;
import lombok.RequiredArgsConstructor;
import org.springframework.core.io.Resource;
import org.springframework.core.io.UrlResource;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import test.chat.attachment.AttachmentService;
import test.chat.statics.StaticWords;

import java.net.MalformedURLException;
import java.nio.file.Path;
import java.nio.file.Paths;


@RequiredArgsConstructor
@RestController
@RequestMapping("/api/attachment")
public class AttachmentController {


    private final AttachmentService attachmentService;
    private final Path root = Paths.get("uploads");

    @GetMapping("/{id}")
    public ResponseEntity<?> get(@PathVariable @NotNull Integer id) {
        try {
            byte[] bytes = attachmentService.attachmentToBytes(id);
            return ResponseEntity.status(200).body(bytes);
        } catch (Exception e) {
            e.printStackTrace();
            return ResponseEntity.status(HttpStatus.EXPECTATION_FAILED).body(e.getMessage());
        }
    }

    @GetMapping("/file1/{filename:.+}")
    public ResponseEntity<?> getFile1(@PathVariable String filename, HttpServletResponse response) {
        try {
            if (filename != null) {
                Resource file = load(StaticWords.DEST + filename);

                return ResponseEntity.ok()
                        .header(HttpHeaders.CONTENT_DISPOSITION, "attachment; filename=\"" + filename + "\"").body(file);
            }else {
                return null;
            }

        } catch (Exception e) {
            e.printStackTrace();
            return ResponseEntity.status(HttpStatus.EXPECTATION_FAILED).body(e.getMessage());
        }
    }

    public Resource load(String filename) {
        try {
            Path file = root.resolve(filename);
            Resource resource = new UrlResource(file.toUri());

            if (resource.exists() || resource.isReadable()) {
                return resource;
            } else {
                throw new RuntimeException("Could not read the file!");
            }
        } catch (MalformedURLException e) {
            throw new RuntimeException("Error: " + e.getMessage());
        }
    }
}
