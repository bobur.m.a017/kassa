package test.chat.controller;

import jakarta.servlet.http.HttpServletRequest;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;
import test.chat.conf.JwtServices;
import test.chat.message.StateMessage;
import test.chat.order.IOrderServices;
import test.chat.order.OrderDTO;
import test.chat.order.TypeOrder;


@RequiredArgsConstructor
@RequestMapping("/api/v1/order")
@RestController
public class OrderController {

    private final IOrderServices orederService;
    private final JwtServices jwtServices;

    @PreAuthorize("hasAnyRole('USER')")
    @PostMapping
    public ResponseEntity<?> addOrder(@RequestBody OrderDTO orderDTO, HttpServletRequest request) {
        String token = jwtServices.getToken(request);
        StateMessage stateMessage = orederService
                .addOrder(orderDTO, TypeOrder.TREASURY.getName(),jwtServices.getOfficeId(token),jwtServices.getUsername(token));
        return ResponseEntity.status(stateMessage.getCode()).body(stateMessage.getText());
    }
    @PreAuthorize("hasAnyRole('DELIVERY_ADMIN')")
    @PostMapping("/delivery")
    public ResponseEntity<?> addOrderDelivery(@RequestBody OrderDTO orderDTO,HttpServletRequest request) {
        String token = jwtServices.getToken(request);
        StateMessage stateMessage = orederService
                .addOrder(orderDTO, TypeOrder.DELIVERY.getName(),jwtServices.getOfficeId(token),jwtServices.getUsername(token));
        return ResponseEntity.status(stateMessage.getCode()).body(stateMessage.getText());
    }
    @PreAuthorize("hasAnyRole('USER','DELIVERY_ADMIN')")
    @PutMapping
    public ResponseEntity<?> editeOrder(@RequestBody OrderDTO orderDTO) {
        StateMessage stateMessage = orederService
                .editeOrder(orderDTO);
        return ResponseEntity.status(stateMessage.getCode()).body(stateMessage.getText());
    }
    @PreAuthorize("hasAnyRole('USER','DELIVERY_ADMIN')")
    @PutMapping("/status/{id}")
    public ResponseEntity<?> editeStatusOrder(@PathVariable Integer id,HttpServletRequest request) {
        String token = jwtServices.getToken(request);
        StateMessage stateMessage = orederService
                .changeStatus(id,jwtServices.getOfficeId(token));
        return ResponseEntity.status(stateMessage.getCode()).body(stateMessage.getText());
    }

    @PreAuthorize("hasAnyRole('DELIVERY_ADMIN','USER')")
    @DeleteMapping("/{id}")
    public ResponseEntity<?> deleteOrder(@PathVariable Integer id) {
        StateMessage stateMessage = orederService
                .deleteOrder(id);
        return ResponseEntity.status(stateMessage.getCode()).body(stateMessage.getText());
    }
    @PreAuthorize("hasAnyRole('DELIVERY_ADMIN','USER')")
    @DeleteMapping("/foodOrder/{id}")
    public ResponseEntity<?> deleteFoodOrder(@PathVariable Integer id) {
        StateMessage stateMessage = orederService
                .deleteFoodOrder(id);
        return ResponseEntity.status(stateMessage.getCode()).body(stateMessage.getText());
    }

    @PreAuthorize("hasAnyRole('ADMIN','SUPER_ADMIN','USER','DELIVERY_ADMIN','DIRECTOR')")
    @GetMapping
    public ResponseEntity<?> getOrders(@RequestParam(required = false) Long startDate,@RequestParam(required = false)  Long endDate,@RequestParam(required = false) String orderType,HttpServletRequest request) {
        String token = jwtServices.getToken(request);
        return ResponseEntity.ok(orederService
                .getOrders(startDate,endDate, orderType,jwtServices.getOfficeId(token)));
    }
    @PreAuthorize("hasAnyRole('ADMIN','SUPER_ADMIN','USER','DELIVERY_ADMIN','DIRECTOR')")
    @GetMapping("/{id}")
    public ResponseEntity<?> getOrder(@PathVariable Integer id) {
        OrderDTO user = orederService
                .getOrder(id);
        if (user != null) {
            return ResponseEntity.ok(user);
        } else {
            return ResponseEntity.status(HttpStatus.NO_CONTENT).body("Not found");
        }
    }

}
