package test.chat.controller;

import jakarta.servlet.http.HttpServletRequest;
import lombok.RequiredArgsConstructor;
import org.apache.tomcat.Jar;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.web.bind.annotation.*;

import test.chat.conf.JwtServices;
import test.chat.message.StateMessage;
import test.chat.user.IUserService;
import test.chat.user.UserDTO;
import test.chat.user.UserSignUpDTO;

@RequiredArgsConstructor
@RequestMapping("/api/v1/user")
@RestController
public class UserController {

    private final IUserService userServices;
    private final JwtServices jwtServices;

    @PostMapping
    public ResponseEntity<?> createUser(@RequestBody UserSignUpDTO userSignUpDTO) {
        StateMessage stateMessage = userServices.addUser(userSignUpDTO);
        return ResponseEntity.status(stateMessage.getCode()).body(stateMessage.getText());
    }
    @PutMapping("/{id}")
    public ResponseEntity<?> editeUser(@PathVariable Integer id, @RequestBody UserSignUpDTO userSignUpDTO) {
        StateMessage stateMessage = userServices.editeUser(id,userSignUpDTO);
        return ResponseEntity.status(stateMessage.getCode()).body(stateMessage.getText());
    }
    @PutMapping("/state")
    public ResponseEntity<?> editeStateUser(@RequestBody UserDTO userSignUpDTO) {
        StateMessage stateMessage = userServices.editeState(userSignUpDTO);
        return ResponseEntity.status(stateMessage.getCode()).body(stateMessage.getText());
    }
    @PostMapping("/{id}")
    public ResponseEntity<?> addUserToRestaurant(@PathVariable Integer id, @RequestBody UserSignUpDTO userSignUpDTO, HttpServletRequest request) {
        String token = jwtServices.getToken(request);
        StateMessage stateMessage = userServices.addUserToRestaurant(userSignUpDTO, id);
        return ResponseEntity.status(stateMessage.getCode()).body(stateMessage.getText());
    }

    @DeleteMapping("/{id}")
    public ResponseEntity<?> deleteUser(@PathVariable Integer id) {
        StateMessage stateMessage = userServices.deleteUser(id);
        return ResponseEntity.status(stateMessage.getCode()).body(stateMessage.getText());
    }

    @GetMapping
    public ResponseEntity<?> getUsers() {
        return ResponseEntity.ok(userServices.getUsers());
    }

    @GetMapping("/{id}")
    public ResponseEntity<?> getUser(@PathVariable Integer id) {
        UserDTO user = userServices.getUser(id);
        if (user != null) {
            return ResponseEntity.ok(user);
        } else {
            return ResponseEntity.status(HttpStatus.NO_CONTENT).body("Not found");
        }
    }

}
