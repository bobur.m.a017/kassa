package test.chat.controller;

import jakarta.servlet.http.HttpServletRequest;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.web.bind.annotation.*;
import test.chat.category.CategoryDTO;
import test.chat.conf.JwtServices;
import test.chat.message.StateMessage;
import test.chat.warehouseOfProducts.IWarehouseProductServices;
import test.chat.warehouseOfProducts.WarehouseOfProductDTO;


@RequiredArgsConstructor
@RequestMapping("/api/v1/warehouse-product")
@RestController
public class WarehouseProductController {

    private final IWarehouseProductServices warehouseProductServices;
    private final JwtServices jwtServices;
    private AuthenticationManager authenticationManager;

    @PreAuthorize("hasAnyRole('ADMIN','SUPER_ADMIN')")
    @PostMapping
    public ResponseEntity<?> addCategory(@RequestBody WarehouseOfProductDTO userSignUpDTO) {
        StateMessage stateMessage = warehouseProductServices.addWarehouseProduct(userSignUpDTO,null);
        return ResponseEntity.status(stateMessage.getCode()).body(stateMessage.getText());
    }
    @PreAuthorize("hasAnyRole('ADMIN','SUPER_ADMIN')")
    @PutMapping
    public ResponseEntity<?> editeCategory(@RequestBody WarehouseOfProductDTO userSignUpDTO) {
        StateMessage stateMessage = warehouseProductServices.editeWarehouseProduct(userSignUpDTO);
        return ResponseEntity.status(stateMessage.getCode()).body(stateMessage.getText());
    }

    @PreAuthorize("hasAnyRole('ADMIN','SUPER_ADMIN')")
    @DeleteMapping("/{id}")
    public ResponseEntity<?> deleteCategory(@PathVariable Integer id) {
        StateMessage stateMessage = warehouseProductServices.deleteWarehouseProduct(id);
        return ResponseEntity.status(stateMessage.getCode()).body(stateMessage.getText());
    }

    @PreAuthorize("hasAnyRole('SUPER_ADMIN','USER','ADMIN','DELIVERY_ADMIN','DIRECTOR')")
    @GetMapping
    public ResponseEntity<?> getCategorys(HttpServletRequest request) {
        String token = jwtServices.getToken(request);
        return ResponseEntity.ok(warehouseProductServices.getWarehouseProducts(jwtServices.getOfficeId(token)));
    }
    @PreAuthorize("hasAnyRole('SUPER_ADMIN','USER','ADMIN','DELIVERY_ADMIN','DIRECTOR')")
    @GetMapping("/{id}")
    public ResponseEntity<?> getCategory(@PathVariable Integer id) {
        WarehouseOfProductDTO warehouse = warehouseProductServices.getWarehouseProduct(id);
        if (warehouse != null) {
            return ResponseEntity.ok(warehouse);
        } else {
            return ResponseEntity.status(HttpStatus.NO_CONTENT).body("Not found");
        }
    }

}
