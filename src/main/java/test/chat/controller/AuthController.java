package test.chat.controller;

import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import lombok.RequiredArgsConstructor;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.util.MultiValueMap;
import org.springframework.web.bind.annotation.*;
import test.chat.conf.CustomAuthentication;
import test.chat.user.TokenResponse;
import test.chat.user.UserSignInDTO;
import test.chat.user.UserSignUpDTO;

import java.util.Enumeration;

@RequiredArgsConstructor
@RequestMapping("/api/v1")
@RestController
public class AuthController {
    private final CustomAuthentication customAuthentication;

    @RequestMapping(
            path = "/auth",
            consumes = MediaType.APPLICATION_FORM_URLENCODED_VALUE,
            method = RequestMethod.POST,
            produces = {
                    MediaType.APPLICATION_JSON_VALUE
            })
    public ResponseEntity<?> authentication(@RequestParam MultiValueMap paramMap, HttpServletRequest request, HttpServletResponse response) {
        System.out.println(paramMap.get("password"));
        TokenResponse tokenResponse = customAuthentication.customAuthentication(request, response);
        return ResponseEntity.ok(tokenResponse);

    }
    @PreAuthorize("hasAnyRole('SUPER_ADMIN')")
    @GetMapping
    public ResponseEntity<?> ok(HttpServletRequest req) {
        String headers = req.getHeader("Authorization");
        System.out.println(headers);
//        TokenResponse tokenResponse = new TokenResponse(customAuthentication.customAuthentication(request, response));
        return ResponseEntity.ok("tokenResponse");

    }
    @PreAuthorize("hasAnyRole('SUPER_ADMIN','USER','ADMIN','DELIVERY_ADMIN','DIRECTOR')")
    @GetMapping("/role")
    public ResponseEntity<?> getRole(HttpServletRequest req) {
        String role = customAuthentication.getRole(req);
        return ResponseEntity.ok(role);

    }
}
