package test.chat.controller;

import jakarta.servlet.http.HttpServletRequest;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;
import test.chat.conf.JwtServices;
import test.chat.message.StateMessage;
import test.chat.warehouseOfProducts.IWarehouseProductServices;
import test.chat.warehouseOfProducts.WarehouseOfProductDTO;


@RequiredArgsConstructor
@RequestMapping("/api/v1/products")
@RestController
public class ProductController {

    private final IWarehouseProductServices productService;
    private final JwtServices jwtServices;

    @PreAuthorize("hasAnyRole('ADMIN','SUPER_ADMIN',T(test.chat.role.RoleType).ROLE_USER.name())")
    @PostMapping
    public ResponseEntity<?> getWarehouseProduct(@RequestBody WarehouseOfProductDTO productDTO, HttpServletRequest request) {
        String token = jwtServices.getToken(request);
        StateMessage stateMessage = productService.addWarehouseProduct(productDTO,jwtServices.getOfficeId(token));
        return ResponseEntity.status(stateMessage.getCode()).body(stateMessage.getText());
    }
    @PreAuthorize("hasAnyRole('ADMIN','SUPER_ADMIN')")
    @PutMapping
    public ResponseEntity<?> edgetWarehouseProduct(@RequestBody WarehouseOfProductDTO productDTO,HttpServletRequest request) {
        String token = jwtServices.getToken(request);
        StateMessage stateMessage = productService.editeWarehouseProduct(productDTO);
        return ResponseEntity.status(stateMessage.getCode()).body(stateMessage.getText());
    }

    @PreAuthorize("hasAnyRole('ADMIN','SUPER_ADMIN')")
    @DeleteMapping("/{id}")
    public ResponseEntity<?> delgetWarehouseProduct(@PathVariable Integer id) {
        StateMessage stateMessage = productService.deleteWarehouseProduct(id);
        return ResponseEntity.status(stateMessage.getCode()).body(stateMessage.getText());
    }

    @PreAuthorize("hasAnyRole('ADMIN','SUPER_ADMIN')")
    @GetMapping
    public ResponseEntity<?> getProducts(HttpServletRequest request) {
        String token = jwtServices.getToken(request);
        return ResponseEntity.ok(productService.getWarehouseProducts(jwtServices.getOfficeId(token)));
    }
    @PreAuthorize("hasAnyRole('ADMIN','SUPER_ADMIN')")
    @GetMapping("/{id}")
    public ResponseEntity<?> getWarehouseProduct(@PathVariable Integer id) {
        WarehouseOfProductDTO user = productService.getWarehouseProduct(id);
        if (user != null) {
            return ResponseEntity.ok(user);
        } else {
            return ResponseEntity.status(HttpStatus.NO_CONTENT).body("Not found");
        }
    }

}
