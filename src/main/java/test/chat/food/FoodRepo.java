package test.chat.food;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;
import test.chat.category.Category;

import java.util.List;

@Repository
public interface FoodRepo extends JpaRepository<Food,Integer> {
    List<Food> findAllByCategory(Category category);
    List<Food> findAllByIdIn(List<Integer> integers);

    @Modifying
    @Query("DELETE FROM Food b WHERE b.id = ?1")
    void deleteByIdFood(Integer id);
}
