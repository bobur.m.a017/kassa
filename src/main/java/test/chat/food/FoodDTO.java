package test.chat.food;

import lombok.*;
import test.chat.category.Category;
import test.chat.productComposition.ProductCompositionDTO;

import java.math.BigDecimal;
import java.util.List;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class FoodDTO {
    private Integer id;
    private String name;
    private String imgUrl;
    private BigDecimal price;
    private String description;
    private Category category;
    private List<ProductCompositionDTO> productCompositionDTOS;
    public FoodDTO(Integer id, String name, BigDecimal price, String description, String imgUrl,Category category,List<ProductCompositionDTO> productCompositionDTOS) {
        this.id = id;
        this.name = name;
        this.imgUrl = imgUrl;
        this.price = price;
        this.description = description;
        this.category = category;
        this.productCompositionDTOS = productCompositionDTOS;
    }
    public FoodDTO(Integer id, String name, BigDecimal price, String description, List<ProductCompositionDTO> productCompositionDTOS) {
        this.id = id;
        this.name = name;
        this.price = price;
        this.description = description;
        this.productCompositionDTOS = productCompositionDTOS;
    }
}
