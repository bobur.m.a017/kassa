package test.chat.food;

import com.fasterxml.jackson.annotation.JsonIgnore;
import jakarta.persistence.*;
import lombok.*;
import test.chat.allClasses.SuperClass;
import test.chat.attachment.Attachment;
import test.chat.category.Category;
import test.chat.foodOrders.FoodOrders;
import test.chat.productComposition.ProductComposition;
import test.chat.restaurant.Restaurant;

import java.math.BigDecimal;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

@Entity
@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
public class Food extends SuperClass {
    private String name;
    private String description;
    @Column(precision = 19, scale = 6)
    private BigDecimal price;
    @Column(precision = 19, scale = 6)
    private BigDecimal bodyPrice= new BigDecimal(0);
    private String imageUrl;

    @OneToMany(fetch = FetchType.EAGER,cascade = CascadeType.ALL)
    @JoinColumn(name = "food_id",referencedColumnName = "id")
    private List<ProductComposition> productCompositions;

    @ManyToOne
    @JsonIgnore
    private Category category;

    @ManyToOne(optional = false)
    @JsonIgnore
    private Restaurant restaurant;

    @OneToOne(cascade = CascadeType.ALL)
    private Attachment attachment;

    public Food(Integer id, String name, String description, BigDecimal price, String imageUrl, Category category) {
        super(id);
        this.name = name;
        this.description = description;
        this.price = price;
        this.imageUrl = imageUrl;
        this.category = category;
    }

    public BigDecimal getBodyPrice() {
            BigDecimal bigDecimal = new BigDecimal(0);
        if (this.productCompositions != null) {
            for (ProductComposition productComposition : this.productCompositions) {
                bigDecimal = bigDecimal.add(productComposition.getWarehouseOfProducts().getPrice().multiply(productComposition.getWeight()));
            }
        }
        return bigDecimal;
    }

    public List<ProductComposition> getProductCompositions() {
       productCompositions.sort(new Comparator<ProductComposition>() {
            @Override
            public int compare(ProductComposition o1, ProductComposition o2) {
                return o1.getWarehouseOfProducts().getName().compareTo(o2.getWarehouseOfProducts().getName());
            }
        });
       return productCompositions;
    }
}
