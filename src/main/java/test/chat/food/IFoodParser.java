package test.chat.food;

import test.chat.category.Category;
import test.chat.foodFromSave.FoodFromSave;
import test.chat.parsers.IParsersServices;
import test.chat.productComposition.ProductCompositionDTO;
import test.chat.statics.StaticWords;

import java.util.List;

public interface IFoodParser extends IParsersServices {
    default FoodDTO foodToDTO(Food food, List<ProductCompositionDTO> productCompositionDTOS) {
        return new FoodDTO(
                food.getId(),
                food.getName(),
                food.getPrice(),
                checkNull(food.getDescription()),
                (food.getAttachment() != null ? food.getAttachment().getName() : null) != null ? StaticWords.URI + (food.getAttachment() != null ? food.getAttachment().getName() : null) : null,
                checkNull(food.getCategory()),
                productCompositionDTOS
        );
    }
    default FoodDTO foodSaveToDTO(FoodFromSave food, List<ProductCompositionDTO> productCompositionDTOS) {
        return new FoodDTO(
                food.getId(),
                food.getName(),
                food.getPrice(),
                checkNull(food.getDescription()),
                productCompositionDTOS
        );
    }

    default Food DTOToFood(FoodDTO food, Category category) {
        return new Food(
                checkNull(food.getId()),
                food.getName(),
                checkNull(food.getDescription()),
                food.getPrice(),
                checkNull(food.getImgUrl()),
                category
        );
    }
}
