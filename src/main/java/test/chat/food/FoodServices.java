package test.chat.food;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.multipart.MultipartFile;
import test.chat.attachment.Attachment;
import test.chat.attachment.AttachmentService;
import test.chat.category.Category;
import test.chat.category.CategoryRepo;
import test.chat.message.StateMessage;
import test.chat.parsers.IParsersServices;
import test.chat.productComposition.IProductCompositionParser;
import test.chat.productComposition.ProductComposition;
import test.chat.productComposition.ProductCompositionDTO;
import test.chat.productComposition.ProductCompositionRepo;
import test.chat.restaurant.Restaurant;
import test.chat.restaurant.RestaurantRepo;
import test.chat.warehouseOfProducts.WarehouseOfProductRepo;
import test.chat.warehouseOfProducts.WarehouseOfProducts;

import javax.imageio.ImageIO;
import java.awt.image.BufferedImage;
import java.io.InputStream;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.*;

@RequiredArgsConstructor
@Service
public class FoodServices implements IFoodServices, IFoodParser, IParsersServices, IProductCompositionParser {
    private final FoodRepo foodRepo;
    private final CategoryRepo categoryRepo;
    private final RestaurantRepo restaurantRepo;
    private final AttachmentService attachmentService;
    private final WarehouseOfProductRepo warehouseOfProductRepo;
    private final ProductCompositionRepo productCompositionRepo;

    @Override
    public StateMessage addFood(FoodDTO foodDTO, Integer restaurantId) {
        Food food = DTOToFood(foodDTO, null);
        food.setRestaurant(restaurantRepo.findById(restaurantId).orElse(null));
        foodRepo.save(food);
        return new StateMessage("Qo'shildi", true, 200);
    }

    @Override
    @Transactional
    public StateMessage addProductCompositionToFood(Integer foodId, ProductCompositionDTO productCompositionDTOS, Integer restaurantId) {
        Food food = foodRepo.findById(foodId).orElse(null);
        WarehouseOfProducts warehouseOfProducts = warehouseOfProductRepo.findById(productCompositionDTOS.getProductDTO().getId()).orElse(null);
        Restaurant restaurant = restaurantRepo.findById(restaurantId).orElse(null);
        if (food == null || warehouseOfProducts == null || restaurant == null) {
            return new StateMessage("Qo'shilmadi", true, 500);
        }
        productCompositionRepo.save(dtoToProductComposition(productCompositionDTOS, food, warehouseOfProducts));
        return new StateMessage("Qo'shildi", true, 200);
    }

    @Override
    @Transactional
    public StateMessage editeProductCompositionToFood(Integer foodId, ProductCompositionDTO productCompositionDTOS) {
        if (productCompositionDTOS.getId() == null) {
            return new StateMessage("Topilmadi", true, 404);
        }
        ProductComposition productComposition = productCompositionRepo.findById(productCompositionDTOS.getId()).orElse(null);
        if (productComposition == null) {
            return new StateMessage("Topilmadi", true, 404);
        }
        productComposition.setWeight(new BigDecimal(productCompositionDTOS.getWeight().toString()).setScale(6, RoundingMode.HALF_EVEN));
        productCompositionRepo.save(productComposition);
        return new StateMessage("O'zgartirildi", true, 200);
    }

    @Override
    public StateMessage addToCategoryFood(FoodDTO foodDTO, Integer id) {
        Category category = categoryRepo.findById(id).orElse(null);
        if (category != null) {
            foodRepo.save(DTOToFood(foodDTO, category));
            return new StateMessage("Qo'shildi", true, 200);
        }
        return new StateMessage("Qo'shilmadi", true, 500);
    }

    @Override
    public StateMessage editeFood(FoodDTO foodDTO, Integer restaurantId) {
        Food food = foodRepo.findById(checkNull(foodDTO.getId())).orElse(null);
        if (food == null) {
            return new StateMessage("O'zgartirilmadi", false, 500);
        }
        food.setName(foodDTO.getName());
        food.setDescription(checkNull(foodDTO.getDescription()));
        food.setImageUrl(checkNull(foodDTO.getImgUrl()));
        food.setPrice(foodDTO.getPrice());
        foodRepo.save(food);
        return new StateMessage("O'zgartirildi", true, 200);
    }
    @Transactional
    @Override
    public StateMessage deleteFood(Integer id) {
        foodRepo.deleteByIdFood(id);
        return new StateMessage("O'chirildi", true, 200);
    }

    @Override
    @Transactional
    public StateMessage deleteProductComposition(Integer id) {
        productCompositionRepo.deleteByIdComp(id);
        return new StateMessage("O'chirildi", true, 200);
    }

    @Override
    public List<FoodDTO> getFoods(Integer id) {
        List<Food> all;
        if (id != null) {
            Category category = categoryRepo.findById(id).orElse(null);
            if (category != null) {
                all = category.getFood();
            } else {
                return new ArrayList<>();
            }
        } else {
            all = foodRepo.findAll();
        }
        Collections.sort(all, new Comparator<Food>() {
            @Override
            public int compare(Food food1, Food food2) {
                return food1.getName().compareTo(food2.getName());
            }
        });
        List<FoodDTO> foodDTOList = new ArrayList<>();
        for (Food food : all) {
            foodDTOList.add(foodToDTO(food, productCompToDTOList(food.getProductCompositions())));
        }
        return foodDTOList;
    }

    @Override
    public FoodDTO getFood(Integer id) {
        Food food = foodRepo.findById(id).orElse(null);
        if (food == null) {
            return null;
        }
        return foodToDTO(food, productCompToDTOList(food.getProductCompositions()));
    }

    @Override
    public StateMessage relationsToCategory(Integer categoryId, Integer foodId) {
        Category category = categoryRepo.findById(categoryId).orElse(null);
        if (category == null) {
            return new StateMessage("Taom turi topilmadi", false, 500);
        }
        Food food = foodRepo.findById(foodId).orElse(null);
        if (food == null) {
            return new StateMessage("Taom topilmadi", false, 500);
        }
        food.setCategory(category);
        foodRepo.save(food);
        return new StateMessage("Taom turiga biriktirildi", true, 200);
    }

    @Override
    public StateMessage addAttachment(Integer id, MultipartFile file) {
        StateMessage stateMessage = new StateMessage().wrongInformation();
        Optional<Food> optionalFood = foodRepo.findById(id);

        try {

            InputStream inputStream = file.getInputStream();
            BufferedImage imageFile = ImageIO.read(inputStream);

            int width = imageFile.getWidth();
            int height = imageFile.getHeight();

            System.out.println("Image width: " + width);
            System.out.println("Image height: " + height);


            if (optionalFood.isPresent()) {

                Food food = optionalFood.get();
                Attachment attachment = attachmentService.add(file);

                if (attachment != null) {
                    food.setAttachment(attachment);
                    foodRepo.save(food);
                    stateMessage = new StateMessage().successAdd();
                }
            }

        } catch (Exception e) {
            return new StateMessage("RASM FORMATI XATO", false, 417);
        }
        return stateMessage;
    }

    public WarehouseOfProducts isEmptyOrWareHouse( Restaurant restaurant) {
//        WarehouseOfProducts warehouseOfProducts = warehouseOfProductRepo.findByProduct_IdAndRestaurant_Id(product.getId(), restaurant.getId()).orElse(null);
//        if (warehouseOfProducts == null) {
//            warehouseOfProducts = warehouseOfProductRepo.save(new WarehouseOfProducts(
//                    null,
//                    BigDecimal.valueOf(0),
//                    BigDecimal.valueOf(0),
//                    BigDecimal.valueOf(0),
//                    restaurant
//            ));
//        }
        return null;
    }
}
