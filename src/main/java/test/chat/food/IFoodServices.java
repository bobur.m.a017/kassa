package test.chat.food;

import org.springframework.web.multipart.MultipartFile;
import test.chat.message.StateMessage;
import test.chat.productComposition.ProductComposition;
import test.chat.productComposition.ProductCompositionDTO;

import java.util.List;

public interface IFoodServices {
    StateMessage addFood(FoodDTO foodDTO,Integer restaurantId);
    StateMessage addProductCompositionToFood(Integer foodId, ProductCompositionDTO productCompositionDTOS,Integer restaurantId);
    StateMessage editeProductCompositionToFood(Integer foodId, ProductCompositionDTO productCompositionDTOS);
    StateMessage addToCategoryFood(FoodDTO foodDTO,Integer id);
    StateMessage editeFood(FoodDTO foodDTO,Integer restaurantId);
    StateMessage deleteFood(Integer id);
    StateMessage deleteProductComposition(Integer id);
    List<FoodDTO> getFoods(Integer id);
    FoodDTO getFood(Integer id);
    StateMessage relationsToCategory(Integer CategoryId,Integer foodId);
    StateMessage addAttachment(Integer id, MultipartFile file);
}
