package test.chat.parsers;
import test.chat.category.Category;
import test.chat.category.CategoryDTO;
import test.chat.food.FoodDTO;

import java.util.List;

public interface IParsersServices {
    default <T> List<T> checkListEmpty(List<T> list){
        if (list.isEmpty()){
            return null;
        }else {
            return list;
        }
    }
    default <T> T checkNull(T obj) {
        if (obj != null) return obj;
        return null;
    }
}
