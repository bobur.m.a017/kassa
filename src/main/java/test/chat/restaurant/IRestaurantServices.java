package test.chat.restaurant;

import test.chat.message.StateMessage;
import test.chat.user.UserDTO;

import java.util.List;

public interface IRestaurantServices {
    StateMessage addRestaurant(RestaurantDTO restaurantDTO,Integer companyId);
    StateMessage addUserToRestaurant(UserDTO userDTO,Integer restaurantId);
    StateMessage editeRestaurant(RestaurantDTO restaurantDTO);
    StateMessage deleteRestaurant(Integer id);
    StateMessage deleteUserInRestaurant(Integer userId);
    RestaurantDTO getRestaurant(Integer restaurantId);
    List<RestaurantDTO> getRestaurantsByCompanyId(Integer restaurantId);
    List<RestaurantDTO> getRestaurants();
    List<RestaurantDTO> getRestaurantsForDirector(Integer id,String username);
    RestaurantDTO getRestaurantsForAdmin(Integer id);
}
