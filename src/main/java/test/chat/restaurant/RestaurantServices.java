package test.chat.restaurant;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import test.chat.company.Company;
import test.chat.company.CompanyRepo;
import test.chat.message.StateMessage;
import test.chat.role.RoleType;
import test.chat.statusWork.StatusWork;
import test.chat.user.UserDTO;
import test.chat.user.UserRepo;
import test.chat.user.Users;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Service
@RequiredArgsConstructor
public class RestaurantServices implements IRestaurantServices, IRestaurantParser {
    private final RestaurantRepo restaurantRepo;
    private final CompanyRepo companyRepo;
    private final UserRepo userRepo;

    @Override
    public StateMessage addRestaurant(RestaurantDTO restaurantDTO, Integer companyId) {
        Company company = companyRepo.findById(companyId).orElse(null);
        if (company != null) {
            restaurantRepo.save(dtoToRestaurant(restaurantDTO, new StatusWork(StatusTypes.MANUAL.getName(), null)));
            return new StateMessage("Qo'shildi", true, 200);
        }
        return new StateMessage("Qo'shilmadi", true, 500);
    }

    @Override
    public StateMessage addUserToRestaurant(UserDTO userDTO, Integer restaurantId) {
        return null;
    }

    @Override
    public StateMessage editeRestaurant(RestaurantDTO restaurantDTO) {
        Optional<Restaurant> restaurantOptional = restaurantRepo.findById(restaurantDTO.getId());
        Restaurant restaurant;
        if (restaurantOptional.isPresent()) {
            restaurant = restaurantOptional.get();
            restaurant.setName(restaurantDTO.getName());
            restaurantRepo.save(restaurant);
            return new StateMessage("O'zgartirildi", true, 200);
        }
        return new StateMessage("O'zgartirilmadi", true, 500);
    }

    @Override
    public StateMessage deleteRestaurant(Integer id) {
        try {
            restaurantRepo.deleteById(id);
            return new StateMessage("O'chirildi", true, 200);
        } catch (Exception e) {
            return new StateMessage("O'chirilmadi", true, 500);
        }
    }

    @Override
    public StateMessage deleteUserInRestaurant(Integer userId) {
        return null;
    }

    @Override
    public RestaurantDTO getRestaurant(Integer restaurantId) {
        Optional<Restaurant> optionalRestaurant = restaurantRepo.findById(restaurantId);
        return null;
    }

    @Override
    public List<RestaurantDTO> getRestaurantsByCompanyId(Integer companyId) {
        restaurantRepo.findAllByCompany_Id(companyId);
        List<Restaurant> restaurants = restaurantRepo.findAll();
        List<RestaurantDTO> restaurantDTOS = new ArrayList<>();
        for (Restaurant restaurant : restaurants) {
            restaurantDTOS.add(restaurantToDTO(restaurant));
        }
        return restaurantDTOS;
    }

    @Override
    public List<RestaurantDTO> getRestaurants() {
        return null;
    }

    @Override
    public RestaurantDTO getRestaurantsForAdmin(Integer id) {
        Restaurant restaurant = restaurantRepo.findById(id).orElse(null);
        if (restaurant == null) {
            return null;
        }
        return restaurantToDTO(restaurant);
    }

    @Override
    public List<RestaurantDTO> getRestaurantsForDirector(Integer id, String username) {
        Users users = userRepo.findByUsername(username).orElse(null);
        if (users == null) {
            return null;
        }
        List<RestaurantDTO> restaurantDTOS = new ArrayList<>();
        for (Restaurant restaurant : restaurantRepo.findAllByCompany_Id(id)) {
            restaurantDTOS.add(restaurantToDTO(restaurant));
        }
        return restaurantDTOS;
    }


}
