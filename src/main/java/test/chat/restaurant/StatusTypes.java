package test.chat.restaurant;


public enum StatusTypes {

    AUTO(1, "Avto boshqaruv"),
    AUTO_MANUAL(2, "Yarim avto boshqaruv"),
    MANUAL(3, "Qo'l bilan boshqaruv");
    private Integer id;
    private String name;

    StatusTypes(int id, String name) {
        this.id = id;
        this.name = name;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
