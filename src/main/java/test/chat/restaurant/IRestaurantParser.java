package test.chat.restaurant;

import test.chat.statics.StaticMethods;
import test.chat.statusWork.StatusWork;
import test.chat.user.UserDTO;
import test.chat.user.Users;

import java.util.ArrayList;
import java.util.List;

public interface IRestaurantParser {
    default Restaurant dtoToRestaurant(RestaurantDTO productDTO, StatusWork statusWork) {
        return new Restaurant(
                productDTO.getName(),
                statusWork
        );
    }

    default RestaurantDTO restaurantToDTO(Restaurant restaurant) {
        return new RestaurantDTO(
                restaurant.getId(),
                restaurant.getName(),
                parseUserDTOS(restaurant.getRestaurantsUser())
        );
    }

    default UserDTO parseUserDto(Users user) {
        UserDTO userDTO = new UserDTO();
        if (user == null) {
            return null;
        }

        userDTO.setId(StaticMethods.checkNull(user.getId()));
        userDTO.setName(StaticMethods.checkNull(user.getName()));
        userDTO.setSurname(StaticMethods.checkNull(user.getSurname()));
        userDTO.setPassword(StaticMethods.checkNull(user.getPassword()));
        userDTO.setUsername(StaticMethods.checkNull(user.getUsername()));
        userDTO.setFatherName(StaticMethods.checkNull(user.getFatherName()));
        userDTO.setState(StaticMethods.checkNull(user.getIsActive()));
        return userDTO;
    }

    default List<UserDTO> parseUserDTOS(List<Users> users) {
        List<UserDTO> userDTOList = new ArrayList<>();
        if (users.isEmpty()) {
            return userDTOList;
        }
        for (Users user : users) {
            userDTOList.add(parseUserDto(user));
        }
        return userDTOList;
    }

}
