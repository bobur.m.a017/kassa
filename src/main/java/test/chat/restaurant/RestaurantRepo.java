package test.chat.restaurant;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;
import test.chat.user.Users;

import java.util.List;
import java.util.Optional;

@Repository
public interface RestaurantRepo extends JpaRepository<Restaurant,Integer> {
//    @Query("SELECT o FROM Restaurant o WHERE o.restaurantsUser > ?1 AND o.updatedDate < ?2")
//    Optional<Restaurant> findByUsersInRestaurant(Users users);
    List<Restaurant> findAllByCompany_Id(Integer id);
}
