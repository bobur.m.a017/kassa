package test.chat.restaurant;

import com.fasterxml.jackson.annotation.JsonIgnore;
import jakarta.persistence.*;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import test.chat.allClasses.SuperClass;
import test.chat.company.Company;
import test.chat.delivery.Delivery;
import test.chat.food.Food;
import test.chat.inoutProduct.InoutProduct;
import test.chat.order.Orders;
import test.chat.statusWork.StatusWork;
import test.chat.statusWork.StatusWorkDTO;
import test.chat.user.Users;
import test.chat.warehouseOfProducts.WarehouseOfProducts;
import test.chat.workDay.WorkDay;

import java.sql.Timestamp;
import java.util.List;

@Entity
@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
public class Restaurant extends SuperClass {
    private String name;
    @ManyToOne
    @JsonIgnore
    private Company company;

    @OneToMany(fetch = FetchType.EAGER, cascade = CascadeType.ALL)
    @JoinColumn(name = "restaurant_id", referencedColumnName = "id")
    private List<Users> restaurantsUser;

    @OneToMany
    @JoinColumn(name = "restaurant_id", referencedColumnName = "id")
    private List<WorkDay> workDays;

    @OneToMany(fetch = FetchType.EAGER)
    @JoinColumn(name = "restaurant_id", referencedColumnName = "id")
    private List<WarehouseOfProducts> warehouseOfProducts;
    @OneToMany
    @JoinColumn(name = "restaurant_id", referencedColumnName = "id")
    private List<Food> foods;

    @OneToMany
    @JoinColumn(name = "restaurant_id", referencedColumnName = "id")
    private List<Orders> orders;

    @OneToMany
    @JoinColumn(name = "restaurant_id", referencedColumnName = "id")
    private List<InoutProduct> inoutProducts;
    @OneToMany
    @JoinColumn(name = "restaurant_id", referencedColumnName = "id")
    private List<Delivery> deliveries;

    @OneToOne(cascade = CascadeType.ALL)
    @JoinColumn(name = "status_work_id", referencedColumnName = "id")
    @JsonIgnore
    private StatusWork statusWork;

    public Restaurant(String name, StatusWork statusWork) {
        this.name = name;
        this.statusWork = statusWork;
    }
}